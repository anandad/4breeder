package com.breeders.fragments


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.activities.ShowImageActivity
import com.breeders.adaptors.ImageAdapter
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_ads_slider.view.*
import kotlinx.android.synthetic.main.fragment_aid_details.view.*
import org.json.JSONObject
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AidDetailsFragment : Fragment(), ServiceEvents {

    lateinit var ads: Models.Ads


    lateinit var fragView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_aid_details, container, false)

        MainActivity.leftIcon.setImageResource(R.drawable.ic_left)
        MainActivity.rightIcon.visibility = View.GONE
        MainActivity.homeTitle.text = "4BREEDERS"


        MainActivity.leftIcon.setOnClickListener {
            activity!!.onBackPressed()
        }
        ads = arguments!!.getSerializable("ads") as Models.Ads
        getSingleAds(ads.id)
        return fragView;
    }

    private fun autoPageChangeStart(mPager: ViewPager, NUM_PAGES: Int) {
        val handler = Handler()
        val Update = object : Runnable {
            override fun run() {
                var currentPage = mPager.getCurrentItem()
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0
                } else {
                    currentPage++
                }
                mPager.setCurrentItem(currentPage, true)
                handler.postDelayed(this, 5000)
            }
        }

        handler.postDelayed(Update, 500)
    }

    @SuppressLint("HardwareIds")
    private fun setUI() {
        // ads = arguments!!.getSerializable("ads") as Models.Ads
        fragView.tv_ads_dec.text = ads.description
        fragView.tv_viewer.text = ads.add_visitor

//        Picasso.get()
//            .load(ads.image)
//            .into(fragView.iv_ads_image)

        var adsImage = ArrayList<String>()

        adsImage.add(ads.image!!)

        if (ads.key != null)
            for (image in ads.key!!) {
                adsImage.add(image)
                Log.e("AdsDetail", "Image Url => " + image)
            }

        if (ads.video!!.isNotEmpty())
            adsImage.add(ads.video!!)

        val mPagerAdapter = ImageAdapter(context!!, adsImage)

        fragView.vp_ads_photos.adapter = mPagerAdapter

        fragView.vp_ads_photos.currentItem = 0

        fragView.tab_ads_layout.setupWithViewPager(fragView.vp_ads_photos, true);

        autoPageChangeStart(fragView.vp_ads_photos, adsImage.size)

        fragView.ll_favourite.setOnClickListener {
            setAdsFav(ads.id, UserPrefs.getSharedPreferences(context, "mobile_id"))
        }
        fragView.ll_call.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + ads.phone)
            startActivity(intent)
        }
        fragView.ll_sms.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("sms:" + ads.phone)
            startActivity(intent)
        }
        fragView.ll_share.setOnClickListener {
            val sharingIntent = Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/html");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, ads.toString());
            startActivity(Intent.createChooser(sharingIntent, "Share using"));
        }

        fragView.tv_count_down.text = ads.postdate

    }

    private fun getParams(id: String?): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["id"] = id!!
        return params
    }

    private fun getParams(id: String?, android_id: String?): Map<String, String> {
        val params = HashMap<String, String>()
        val fav = if (ads.fav.equals("0")) "1" else "0"
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["id"] = id!!
        params["mob_id"] = android_id!!
        params["Fav"] = fav
        return params
    }

    private fun getSingleAds(id: String?) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.singleAds, getParams(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun setAdsFav(id: String?, android_id: String?) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.Favourite, getParams(id, android_id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {


        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            if (methodName.contains(Constant.singleAds)) {
                ads = Gson().fromJson(jsonObject.optJSONObject("data").toString(), Models.Ads::class.java)
                setUI()
            } else if (methodName.contains(Constant.Favourite)) {
                Utils.showDialog(context!!, "", jsonObject.optString("message"))
            }
        }

        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {


    }

}
