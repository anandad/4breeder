package com.breeders.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.breeders.R
import com.breeders.Utils.NotificationUtils
import com.breeders.app.Config
import com.breeders.fragments.*
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var mRegistrationBroadcastReceiver: BroadcastReceiver
    private val TAG = "MainActivity"


    companion object {
        lateinit var leftIcon: ImageView
        lateinit var rightIcon: ImageView
        lateinit var homeTitle: TextView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.frag_container, HomeFragment()).commit()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.nav_home
        leftIcon = iv_home_button
        rightIcon = iv_search_button
        homeTitle = tv_home_title

        mRegistrationBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent:  Intent?) {

                // checking for type intent filter
                if (intent!!.action.equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if(intent!!.action.equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    val message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //                 txtMessage.setText(message);
                }
            }
        };
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private fun displayFirebaseRegId() {
        val pref = applicationContext.getSharedPreferences(Config.SHARED_PREF, 0)
        val regId = pref.getString("regId", null)

        Log.e(TAG, "Firebase reg id: $regId")
    }

    override fun onResume() {
        super.onResume()

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mRegistrationBroadcastReceiver,
            IntentFilter(Config.REGISTRATION_COMPLETE)
        )

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mRegistrationBroadcastReceiver,
            IntentFilter(Config.PUSH_NOTIFICATION)
        )

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(applicationContext)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver)
        super.onPause()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.nav_home -> {
                supportFragmentManager.beginTransaction().replace(R.id.frag_container, HomeFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_bid -> {
                // message.setText(R.string.title_dashboard)
                supportFragmentManager.beginTransaction().replace(R.id.frag_container, BidsListFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_add_bid -> {
                //   message.setText(R.string.title_notifications)
                //  showDialog()
                supportFragmentManager.beginTransaction().replace(R.id.frag_container, AddBidFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_directory -> {
                //   message.setText(R.string.title_notifications)
                //  showDialog()
                supportFragmentManager.beginTransaction().replace(R.id.frag_container, DirectoryFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_more -> {
                supportFragmentManager.beginTransaction().replace(R.id.frag_container, MoreFragment()).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun showDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
// ...Irrelevant code for customizing the buttons and title
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.add_bid_dialog, null)
        dialogBuilder.setView(dialogView)

//        val editText = dialogView.findViewById(R.id.label_field) as EditText
//        editText.setText("test label")
        val alertDialog = dialogBuilder.create()
        alertDialog.window.setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show()
    }

    fun popFragment(): Boolean {
        var isPop = false

        val currentFragment = supportFragmentManager
            .findFragmentById(R.id.frag_container)

        if (supportFragmentManager.backStackEntryCount > 0) {
            isPop = true
            supportFragmentManager.popBackStackImmediate()
        }

        return isPop
    }

    override fun onBackPressed() {
        if (!popFragment()) {
            finish()
        }
    }

    private val handleAppCrash = Thread.UncaughtExceptionHandler { thread, ex ->
        Log.e("error", ex.toString())
        //send email here
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("ak79382@gamil.com"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Crash Report")
        i.putExtra(Intent.EXTRA_TEXT, ex.toString())
        try {
            startActivity(Intent.createChooser(i, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }

    }
}
