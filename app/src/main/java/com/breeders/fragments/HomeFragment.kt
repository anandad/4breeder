package com.breeders.fragments


import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.adaptors.HomeAdaptor
import com.breeders.models.Models
import com.breeders.models.SliderPagerAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment(), ServiceEvents {


    private var category =  ArrayList<Models.Category>()
    private var specialCat =  ArrayList<Models.Category>()
    private var slider =  ArrayList<String>()
    private var sliderURL =  ArrayList<String>()
    lateinit var mSectionsPagerAdapter: SliderPagerAdapter
    lateinit var catAdaptor: HomeAdaptor
    lateinit var specialAdaptor: HomeAdaptor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragView =  inflater.inflate(R.layout.fragment_home, container, false)

        Constant.setFlag(context!!)

        getSliders()

        specialAdaptor = HomeAdaptor(context!!, specialCat)

        fragView.ehgv_first_list.adapter = specialAdaptor

        fragView.ehgv_first_list.setOnItemClickListener { adapterView, view, i, l ->
            val subCatFragment = SubCategoryFragment()
            val argument = Bundle()
            argument.putString("id",specialCat[i].id)
            subCatFragment.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, subCatFragment).commit()
        }

        fragView.ehgv_first_list.setExpanded(true);

        catAdaptor = HomeAdaptor(context!!, category)

        fragView.ehgv_second_list.adapter = catAdaptor

        fragView.ehgv_second_list.setOnItemClickListener { adapterView, view, i, l ->
            val subCatFragment = SubCategoryFragment()
            val argument = Bundle()
            argument.putSerializable("id",category[i].id)
            subCatFragment.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, subCatFragment).commit()
        }

        fragView.ehgv_second_list.setExpanded(true);


        mSectionsPagerAdapter = SliderPagerAdapter(fragmentManager!!, slider, sliderURL)


        fragView.vp_photos.setAdapter(mSectionsPagerAdapter)

        fragView.vp_photos.setCurrentItem(0)

        fragView.tab_layout.setupWithViewPager(fragView.vp_photos, true);
        autoPageChangeStart(fragView.vp_photos, 3)

        val literals = JSONObject(UserPrefs.getSharedPreferences(context, "literals"))

        fragView.tv_first_title.text = literals.optString("First Categeory Text")
        fragView.tv_second_title.text = literals.optString("second Categeory Text")

        return fragView;
    }

    private fun getLangParam(): Map<String, String>
    {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        return param
    }


    private fun getSliders() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.slider, null, true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getSpecialCat() {

            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.specialCat, getLangParam(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
    }

    private fun getCategory() {


            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.categoryList, getLangParam(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {

            if (methodName.contains(Constant.slider)) {

                slider.clear()
                sliderURL.clear()

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    slider.add(dataArray.optJSONObject(i).optString("image"))
                    sliderURL.add(dataArray.optJSONObject(i).optString("URL"))
                }
                mSectionsPagerAdapter.notifyDataSetChanged()

                getSpecialCat()

            }

            if (methodName.contains(Constant.specialCat)) {

                specialCat.clear()

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    specialCat.add(Gson().fromJson(dataArray.optJSONObject(i).toString(),Models.Category::class.java))
                }
                specialAdaptor.notifyDataSetChanged()

                getCategory()
            }
            if (methodName.contains(Constant.category)) {

                category.clear()

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    category.add(Gson().fromJson(dataArray.optJSONObject(i).toString(),Models.Category::class.java))
                }
                catAdaptor.notifyDataSetChanged()
            }
        }

        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }



    private fun autoPageChangeStart(mPager: ViewPager, NUM_PAGES: Int)
    {
        val handler = Handler()
        val Update = object : Runnable {
            override fun run() {
                var currentPage = mPager.getCurrentItem()
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0
                } else {
                    currentPage++
                }
                mPager.setCurrentItem(currentPage, true)
                handler.postDelayed(this, 5000)
            }
        }

        handler.postDelayed(Update, 500)
    }


}
