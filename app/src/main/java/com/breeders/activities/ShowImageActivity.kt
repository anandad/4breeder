package com.breeders.activities

import android.app.Activity
import android.os.Bundle
import com.breeders.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_show_image.*

class ShowImageActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_image)
        val imageUrl = intent.getStringExtra("imageUrl")
        val imageNumber = intent.getIntExtra("imageNumber", 0)
        val imageSize = intent.getIntExtra("imageSize", 0)
        Picasso.get()
            .load(imageUrl)
            .placeholder(R.mipmap.placeholder)
            .into(iv_image)
        tv_number.text = "$imageNumber / $imageSize"
        iv_back.setOnClickListener {
            finish()
        }
    }
}
