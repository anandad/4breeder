package com.breeders.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R
import com.breeders.models.CircleTransform
import com.breeders.models.Models
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_directory.view.*


class DirectoryAdaptor internal constructor(var context: Context, var directoryList: ArrayList<Models.Directory>) : BaseAdapter() {

    override fun getCount(): Int {
        return directoryList.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {

        val itemView =   LayoutInflater.from(context).inflate(R.layout.item_directory,null)

        itemView.tv_name.text = directoryList[i].Name
        itemView.tv_tag.text = directoryList[i].tag
       // Picasso.get().load(directoryList[i].flag).into(itemView.iv_flag_image)

        Picasso.get().load(directoryList[i].image).into(itemView.iv_image,
            object : com.squareup.picasso.Callback {
            override fun onSuccess() {
                itemView.pb_loading.visibility = View.GONE
            }

            override fun onError(e: Exception?) {
                itemView.pb_loading.visibility = View.GONE
 //               TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })



        return itemView
    }


}
