package com.breeders.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R
import com.breeders.models.Models
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_categories.view.*


class HomeAdaptor internal constructor(var context: Context, var category: ArrayList<Models.Category>) : BaseAdapter() {

    override fun getCount(): Int {
        return category.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_categories,null)
        itemView.tv_cat_name.text = category[i].title
        Picasso.get()
            .load(category[i].image)
            .into(itemView.iv_cat_image)
        return itemView
    }


}
