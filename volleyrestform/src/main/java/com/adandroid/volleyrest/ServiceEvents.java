package com.adandroid.volleyrest;

public interface ServiceEvents {

    public void StartedRequest();
    public void Finished(String methodName, String response);
    public void FinishedWithException(String methodName, String error);
    public void EndedRequest();
}