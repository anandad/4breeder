package com.breeders.adaptors

import android.content.Context
import android.content.Intent
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.activities.PlayVideoActivity
import com.breeders.activities.ShowImageActivity
import com.squareup.picasso.Picasso
import com.squareup.picasso.Request
import com.squareup.picasso.RequestHandler
import kotlinx.android.synthetic.main.fragment_detail.view.*
import java.io.IOException

import java.util.ArrayList

class ImageAdapter(var context: Context, var images: ArrayList<String>) :
    PagerAdapter() {
    var mLayoutInflater: LayoutInflater

    init {
        mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as RelativeLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = mLayoutInflater.inflate(R.layout.fragment_detail, container, false)

        val imageView = itemView.findViewById<View>(R.id.detail_image) as ImageView
        if (images[position].contains(".png") || images[position].contains(".jpg")
            || images[position].contains(".jpeg")
        ) {

            Picasso.get()
                .load(images[position])
                .placeholder(R.mipmap.placeholder)
                .into(imageView,  object : com.squareup.picasso.Callback {
                    override fun onSuccess() {
                        itemView.pb_loading.visibility = View.GONE
                    }

                    override fun onError(e: Exception?) {
                        itemView.pb_loading.visibility = View.GONE
      //                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                })
        } else {

            val videoRequestHandler = VideoRequestHandler()

            val picassoInstance = Picasso.Builder(context!!.applicationContext)
                .addRequestHandler(videoRequestHandler)
                .build();

            picassoInstance.load(videoRequestHandler.SCHEME_VIDEO + ":" + images[position])
                .placeholder(R.mipmap.video_placeholder)
                .into(imageView)
        }

        itemView.setOnClickListener {
            if (images[position].contains(".png") || images[position].contains(".jpg")
                || images[position].contains(".jpeg")
            ) {

                val intent = Intent(context, ShowImageActivity::class.java)
                intent.putExtra("imageUrl", images[position])
                intent.putExtra("imageNumber", (position + 1))
                intent.putExtra("imageSize", images.size)
                context.startActivity(intent)
            } else {
                val intent = Intent(context, PlayVideoActivity::class.java)
                intent.putExtra("videoUrl", images[position])
                context.startActivity(intent)
            }
        }

        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    inner class VideoRequestHandler : RequestHandler() {
        var SCHEME_VIDEO = "video"
        override fun canHandleRequest(data: Request): Boolean {
            val scheme = data.uri.getScheme()
            return SCHEME_VIDEO == scheme
        }

        @Throws(IOException::class)
        override fun load(data: Request, arg1: Int): RequestHandler.Result {
            val bm = ThumbnailUtils.createVideoThumbnail(data.uri.path, MediaStore.Images.Thumbnails.MINI_KIND)
            return RequestHandler.Result(bm, Picasso.LoadedFrom.DISK)
        }
    }
}
