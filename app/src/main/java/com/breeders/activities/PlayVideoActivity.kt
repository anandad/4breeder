package com.breeders.activities

import android.app.Activity
import android.os.Bundle
import android.widget.MediaController
import com.breeders.R
import kotlinx.android.synthetic.main.activity_paly_video.*

class PlayVideoActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paly_video)
        val videoUrl = intent.getStringExtra("videoUrl")
        vv_play.setVideoPath(videoUrl);
        vv_play.start();
        val ctlr = MediaController(this);
        ctlr.setMediaPlayer(vv_play);
        vv_play.setMediaController(ctlr);
        vv_play.requestFocus();

        iv_back.setOnClickListener {
            finish()
        }

    }
}
