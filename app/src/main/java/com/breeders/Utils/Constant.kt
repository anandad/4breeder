package com.breeders.Utils

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.breeders.R
import com.breeders.activities.MainActivity
import com.breeders.fragments.SearchAdsFragment
import com.breeders.fragments.SettingFragment

object Constant {

   // val baseUrl = "http://sachinsam.com/4breeders/Api/"

    val baseUrl = "https://4breeders.com/Api/"

    val imageUrl = "https://4breeders.com/attachments/shop_images/"

    public val options = baseUrl + "Options"

    val getCountry = baseUrl + "Country/get_country"

    val countryDirectory = baseUrl + "Countrydirectory"

    val slider = baseUrl + "Slider"

    val bid = baseUrl + "Bid"

    val singleComment = baseUrl + "Singlecomment"

    val comment = baseUrl + "Comment"

    val category = baseUrl + "Categeory"

    val specialCat = baseUrl + "Specialcat"

    val categoryList = baseUrl + "Categeorylist"

   // val categorySubList = baseUrl + "Categeorysublist"

    val subCategory = baseUrl + "Subcategeory"

    val categorySubList = baseUrl + "Categeorysublist/getcategeory"

    val getAds = baseUrl + "Ads/getAds"

    val getSpecialAds = baseUrl + "Specialads/getspecialAds"

    val subCategoryAds = baseUrl + "SubcategeoryAds/Subcategeoryads"

    val categoryAds = baseUrl + "CategeoryAds/Ads"



    val addAds = baseUrl + "Addads/addads"

    val updateAds = baseUrl + "Editads/editads"

    val Contact = baseUrl + "Contact"

    val About = baseUrl + "About"

    val livechat = baseUrl + "Privacy/Livechat"

  //  val categoryAds = baseUrl + "CategeoryAds"

    val myAds = baseUrl + "Myads/myads"

    val myFavouriteList = baseUrl + "Favouritelist"

    val Favourite = baseUrl + "Favourite"

    val singleAds = baseUrl + "Singleads/getsingleads"

    val singleBid = baseUrl + "Singlebid"

    val getdirectory = baseUrl + "Directorylist/getdirectory"

    val getSingledirectory = baseUrl + "Singledirectory/getsingledirectory"

    fun getCountryFlag(context: Context): Int {
        when (UserPrefs.getSharedPreferences(context, "language").toLowerCase()) {
            "ru" -> return R.mipmap.russia_flag
            "en" -> return R.mipmap.usa_flag
            "ar" -> return R.mipmap.kuwait_flag
            else -> return R.mipmap.usa_flag
        }
    }

    fun setFlag(context: Context) {
        MainActivity.leftIcon.setImageResource(getCountryFlag(context))
        MainActivity.rightIcon.visibility = View.VISIBLE
        MainActivity.homeTitle.text = context.resources.getString(R.string.app_name)
        MainActivity.leftIcon.setOnClickListener {

        }
        MainActivity.rightIcon.setOnClickListener {
            (context as AppCompatActivity).supportFragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, SearchAdsFragment()).commit()
        }
        MainActivity.leftIcon.setOnClickListener {
            (context as AppCompatActivity).supportFragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, SettingFragment()).commit()
        }
    }

    fun setBack(activity: Activity,title: String) {
        MainActivity.leftIcon.setImageResource(R.drawable.ic_left)
        MainActivity.rightIcon.visibility = View.GONE
        MainActivity.homeTitle.text = title

        MainActivity.leftIcon.setOnClickListener {
            activity.onBackPressed()
        }
    }





}
