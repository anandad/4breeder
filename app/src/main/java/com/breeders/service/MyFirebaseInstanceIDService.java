package com.breeders.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.adandroid.mobileinfo.utils.Utils;
import com.adandroid.volleyrest.GetServices;
import com.adandroid.volleyrest.ServiceEvents;
import com.breeders.R;
import com.breeders.Utils.Constant;
import com.breeders.Utils.UserPrefs;
import com.breeders.app.Config;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements ServiceEvents {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "Token : " + token);

        String fcmUrl = "https://4breeders.com/Api/Fcm";

        if (Utils.isOnline(this)) {
            GetServices bzzzLoginServices = new GetServices(this, this);
            try {
                bzzzLoginServices.RestCallAsync(fcmUrl, getLangParams(token),false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else
            Utils.showDialog(
                    this, "",
                    getString(R.string.no_network));

    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }

    private Map getLangParams(String token) {
        Map params = new HashMap<String, String>();
        params.put("fcm_id", token);
        return params;
    }

    @Override
    public void StartedRequest() {

    }

    @Override
    public void Finished(String methodName, String response) {

    }

    @Override
    public void FinishedWithException(String methodName, String error) {

    }


    @Override
    public void EndedRequest() {

    }
}

