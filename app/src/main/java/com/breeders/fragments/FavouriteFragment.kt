package com.breeders.fragments


import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents

import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.adaptors.AdsAdaptor

import com.breeders.models.Models
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_my_aid.view.*
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FavouriteFragment : Fragment(), ServiceEvents {

    private var adsList = ArrayList<Models.Ads>()
    lateinit var adsAdaptor: AdsAdaptor
    lateinit var fragView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_my_aid, container, false)

        MainActivity.leftIcon.setImageResource(R.drawable.ic_left)
        MainActivity.rightIcon.visibility = View.GONE
        MainActivity.homeTitle.text = "My Favourites"


        MainActivity.leftIcon.setOnClickListener {
            activity!!.onBackPressed()
        }

        adsAdaptor = AdsAdaptor(context!!, adsList)

        fragView.lv_bids_list.adapter = adsAdaptor
        //   fragView.lv_bids_list.adapter = FavouriteAdaptor(context!!)

        getMyFavAds(UserPrefs.getSharedPreferences(context,"mobile_id"))

        return fragView;

    }

    private fun getAdsParam(id: String?): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("mob_id", id!!)
        return param
    }

    private fun getMyFavAds(id: String?) {
        if (Utils.isOnline(context!!)) {

            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.myFavouriteList, getAdsParam(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            when {
                methodName.contains(Constant.myFavouriteList) -> {
                    adsList.clear()

                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        adsList.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Ads::class.java))
                    }
                    adsAdaptor.notifyDataSetChanged()
                }
            }
        }
        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }

}
