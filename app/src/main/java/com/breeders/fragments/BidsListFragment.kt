package com.breeders.fragments


import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.WebViewActivity
import com.breeders.adaptors.BidsAdaptor
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_bids_list.view.*
import kotlinx.android.synthetic.main.item_banner.view.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class BidsListFragment : Fragment(), ServiceEvents {

    private var bids = ArrayList<Models.Bid>()
    lateinit var bidsAdaptor: BidsAdaptor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val fragView =  inflater.inflate(R.layout.fragment_bids_list, container, false)

        getBid()

        Constant.setFlag(context!!)

        bidsAdaptor = BidsAdaptor(context!!, bids)

        fragView.lv_bids_list.adapter = bidsAdaptor

        fragView.lv_bids_list.setOnItemClickListener { adapterView, view, i, l ->
            val bidDetailsFragment = BidDetailsFragment()
            val argument = Bundle()
            argument.putSerializable("bid", bids[i-1])
            bidDetailsFragment.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null)
                .replace(R.id.frag_container, bidDetailsFragment).commit()
        }

        val header = LayoutInflater.from(context).inflate(R.layout.item_banner, null)

        Picasso.get().load("https://4breeders.com/banner2.png").into(header.iv_banner)

        header.iv_banner.setOnClickListener {
            val url = "https://4breeders.com/banner4.php"
//            val i = Intent(Intent.ACTION_VIEW)
//            i.data = Uri.parse(url)
//            startActivity(i)
            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra("url", url)
            startActivity(intent)
        }

        fragView.lv_bids_list.addHeaderView(header)

        return fragView;
    }

    private fun getBidParams(): Map<String, String>
    {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        return params
    }

    private fun getBid() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.bid, getBidParams(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {

            bids.clear()

            val dataArray = jsonObject.optJSONArray("data")

            for (i in 0 until dataArray.length()) {
                bids.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Bid::class.java))
            }
            bidsAdaptor.notifyDataSetChanged()
        }

        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {


    }

}
