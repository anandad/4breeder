package com.adandroid.volleyrest;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.Window;

import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class GetServices {

    private Dialog dialog;

    private boolean isShowing = false;

    public String url = "";
    public String TAG = "GetServices";
    public int timeOut = 120000;
    public ServiceEvents eventHandler;
    public static double MAXMB = 1024 * 1024 * 2;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    Context context;

    public GetServices() {
    }

    public GetServices(ServiceEvents eventHandler, Context context) {
        this.eventHandler = eventHandler;
        this.context = context;
    }

    public GetServices(ServiceEvents eventHandler, String url) {
        this.eventHandler = eventHandler;
        this.url = url;
    }

    public GetServices(ServiceEvents eventHandler, String url,
                       int timeOutInSeconds) {
        this.eventHandler = eventHandler;
        this.url = url;
        this.setTimeOut(timeOutInSeconds);
    }

    public void setTimeOut(int seconds) {
        this.timeOut = seconds * 1000;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void RestCallAsync(final String url, final Map params, final boolean isShowDialog) throws Exception {
        this.url = url;
        eventHandler.StartedRequest();

        if (isShowDialog)
            setProgressDialogShow();

        Log.d(TAG, "Request => " + url + " " + params);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, rl, re) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 10000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 10000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);


    }

    public void multiPartRestCallAsync(final String url, final Map request, final ArrayList<File> imageFiles,
                                       final File video) throws Exception {

        Log.d(TAG, "Request is  =>" + url + " params => " + request.toString());
        this.url = url;
        eventHandler.StartedRequest();

        MultipartRequest multipartRequest = new MultipartRequest(url, re, rl, imageFiles, video, request, context);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        multipartRequest.setRetryPolicy(new

                DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(multipartRequest);
    }

    Response.ErrorListener re = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            //	Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            Log.e(TAG, "Error mgs::> " + error.toString());
            setProgressDialogHide();
            eventHandler.EndedRequest();
            eventHandler.FinishedWithException(url, error.getMessage());
        }
    };
    Response.Listener rl = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            setProgressDialogHide();
            eventHandler.EndedRequest();
            if (response != null) {
                Log.d(TAG, "Response is " + response);
                eventHandler.Finished(url, response);
            }
        }
    };

    private void setProgressDialogShow()
    {
        setProgressDialogHide();
        dialog = new Dialog(context, android.R.style.Theme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.color.colorTransparent);
        dialog.setContentView(R.layout.dialoag_progress);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void setProgressDialogHide() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

}
