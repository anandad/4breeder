package com.breeders.fragments


import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.activities.ShowImageActivity
import com.breeders.adaptors.ImageAdapter
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.add_bid_dialog.view.*
import kotlinx.android.synthetic.main.fragment_bid_details.view.*
import kotlinx.android.synthetic.main.fragment_bid_details.view.tv_bid_price
import kotlinx.android.synthetic.main.item_bidder.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class BidDetailsFragment : Fragment(), ServiceEvents {

    private var bids = ArrayList<Models.Bid>()

    lateinit var fragView: View

    lateinit var dialogView: View

    lateinit var alertDialog: AlertDialog

    lateinit var bid: Models.Bid

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_bid_details, container, false)

        bid = arguments!!.getSerializable("bid") as Models.Bid

        getSingleBid(bid.id!!)

        Constant.setBack(activity as AppCompatActivity, "4BREEDERS")

//        fragView.lv_bidder_list.adapter = AidBidderAdaptor(context!!)
        fragView.iv_add_bid.setOnClickListener {
            showDialog()
        }

        return fragView;
    }

    private fun setUI()
    {
        getBidComment(bid.id!!)
        val literals = JSONObject(UserPrefs.getSharedPreferences(context, "literals"))

        fragView.tv_remaining_time.text = literals.optString("Remaining Time")
        fragView.tv_starting_cost.text = literals.optString("Starting Cost")

        fragView.tv_bid_name.text = bid.bid_name
        fragView.tv_bid_type.text = bid.bid_type
        fragView.tv_bid_dec.text = bid.description
        fragView.tv_bid_price.text = bid.price

//        Picasso.get()
//            .load(bid.image)
//            .resize(400, 300)
//            .into(fragView.iv_bid_image)

        var adsImage = ArrayList<String>()

        adsImage.add(bid.image!!)

        if (bid.gallery != null)
            for (image in bid.gallery!!) {
                adsImage.add(image)
                Log.e("AdsDetail", "Image Url => " + Constant.imageUrl + image)
            }

        val mPagerAdapter = ImageAdapter(context!!, adsImage)

        fragView.vp_bid_photos.adapter = mPagerAdapter

        fragView.vp_bid_photos.currentItem = 0

        fragView.tab_bid_layout.setupWithViewPager(fragView.vp_bid_photos, true);

        autoPageChangeStart(fragView.vp_bid_photos, adsImage.size)

        fragView.iv_bid_image.setOnClickListener {
            val intent = Intent(context, ShowImageActivity::class.java)
            intent.putExtra("imageUrl", bid.image)
            intent.putExtra("imageNumber",1)
            intent.putExtra("imageSize",1)
            context!!.startActivity(intent)
        }

        val format = SimpleDateFormat("dd-MM-yyyy HH", Locale.ENGLISH)
        val date = format.parse(bid.Day+" "+bid.Start_Time)

        val last = date.time

        val current = Date().time
//
        Log.e("BidDetail"," Last Date => "+date)
//        Log.e("BidDetail"," Current Date => "+Date())


        object : CountDownTimer(last - current, 1000) {

            override fun onTick(diff: Long) {
                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000)
                fragView.tv_count_down.text = "$diffHours:$diffMinutes:$diffSeconds"
            }

            override fun onFinish() {
                //   mTextField.setText("done!")
            }
        }.start()
    }

    private fun autoPageChangeStart(mPager: ViewPager, NUM_PAGES: Int) {
        val handler = Handler()
        val Update = object : Runnable {
            override fun run() {
                var currentPage = mPager.getCurrentItem()
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0
                } else {
                    currentPage++
                }
                mPager.setCurrentItem(currentPage, true)
                handler.postDelayed(this, 5000)
            }
        }

        handler.postDelayed(Update, 500)
    }

    private fun showDialog() {
        val dialogBuilder = AlertDialog.Builder(context!!)
// ...Irrelevant code for customizing the buttons and title
        val inflater = this.layoutInflater
        dialogView = inflater.inflate(R.layout.add_bid_dialog, null)
        dialogBuilder.setView(dialogView)

//        val editText = dialogView.findViewById(R.id.label_field) as EditText
//        editText.setText("test label")

        alertDialog = dialogBuilder.create()
        alertDialog.window.setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show()

        dialogView.iv_cross_icon.setOnClickListener {
            alertDialog.dismiss()
        }
        dialogView.btn_add_bid.setOnClickListener {
            if (!isEmpty())
                setComment(bid.id!!)
        }
    }

    private fun isEmpty(): Boolean {
        if (dialogView.et_name.text.trim().isEmpty()) {
            dialogView.et_name.error = getString(R.string.please_enter_name)
            return true
        } else if (dialogView.et_price.text.trim().isEmpty()) {
            dialogView.et_price.error = getString(R.string.please_enter_price)
            return true
        } else if (dialogView.et_phone.text.trim().isEmpty()) {
            dialogView.et_phone.error = getString(R.string.please_enter_phone_number)
            return true
        } else if (dialogView.et_dec.text.trim().isEmpty()) {
            dialogView.et_dec.error = getString(R.string.please_enter_dec)
            return true
        } else if (Integer.parseInt(dialogView.et_price.text.toString()) < Integer.parseInt(bid.price)) {
            Utils.showDialog(context!!, "Alert!", "hey! \n Your bid must greater than starting bid")
            return true
        } else return false
    }

    private fun getBidParams(id: String): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["id"] = id
        return params
    }

    private fun getBidComment(id: String) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.singleComment, getBidParams(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getCommentParams(id: String): Map<String, String> {
        val params = HashMap<String, String>()

        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["post_id"] = id
        params["description"] = dialogView.et_dec.text.toString()
        params["price"] = dialogView.et_price.text.toString()
        params["phone"] = dialogView.et_phone.text.toString()
        params["name"] = dialogView.et_name.text.toString()

        return params
    }

    private fun getParams(id: String?): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["id"] = id!!
        return params
    }

    private fun setComment(id: String) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.comment, getCommentParams(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getSingleBid(id: String?) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.singleBid, getParams(id),true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            if (methodName.contains(Constant.singleBid)) {
                bid = Gson().fromJson(jsonObject.optJSONObject("data").toString(), Models.Bid::class.java)
                setUI()
            }

            else if (methodName.contains(Constant.singleComment, true)) {
                bids.clear()

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {

                    val layout = LayoutInflater.from(context).inflate(R.layout.item_bidder, null, false)
                    layout.tv_bid_price.text = dataArray.optJSONObject(i).optString("price")
                    layout.tv_bidder_name.text = dataArray.optJSONObject(i).optString("name")
                    layout.tv_bidder_dec.text = dataArray.optJSONObject(i).optString("description")
                    fragView.ll_bids.addView(layout)
                }
            } else if (methodName.contains(Constant.comment, true)) {
                if (alertDialog.isShowing)
                    alertDialog.dismiss()
                Utils.showDialog(context!!, "", jsonObject.optString("message"))
                val bidDetailsFragment = BidDetailsFragment()
                val argument = Bundle()
                argument.putSerializable("bid", bid)
                bidDetailsFragment.arguments = argument
                fragmentManager!!.beginTransaction().addToBackStack(null)
                    .replace(R.id.frag_container, bidDetailsFragment).commit()
            }
        }  else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {


    }

}
