package com.breeders.models

import java.io.Serializable

class Models {
    inner class Bid : Serializable {
        var id: String? = ""
        var visitor: String? = ""
        var image: String? = ""
        var bid_name: String? = ""
        var bid_type: String? = ""
        var price: String? = ""
        var Start_Time: String? = ""
        var Day: String? = ""
        var abbr: String? = ""
        var url: String? = ""
        var for_id: String? = ""
        var description: String? = ""
        var folder: String? = null
        var gallery: List<String>? = null
    }

    inner class Bidder : Serializable {
        var id: String? = ""
        var post_id: String? = ""
        var name: String? = ""
        var price: String? = ""
        var phone: String? = ""
        var lang: String? = ""
        var description: String? = ""
    }

    inner class Category : Serializable {
        var id: String? = ""
        var title: String? = ""
        var url: String? = ""
        var time: String? = ""
        var image: String? = ""
        var lang: String? = ""
        var description: String? = ""
    }
//    inner class Ads: Serializable
//    {
//        var id: String? = null
//        var status: String? = null
//        var visitor: String? = null
//        var image: String? = null
//        var subcatId: String? = null
//        var video: String? = null
//        var title: String? = null
//        var description: String? = null
//        var phone: String? = null
//        var abbr: String? = null
//        var url: String? = null
//        var forId: String? = null
//        var succategeory: Any? = null
//    //    var key: List<String>? = null
//    }

    inner class Ads : Serializable {

        var id: String? = null
        var folder: String? = null
        var key: List<String>? = null
        var image: String? = null
        var video: String? = null
        var shopCategorie: String? = null
        var subcatId: String? = null
        var position: String? = null
        var description: String? = null
        var url: String? = null
        var subFor: String? = null
        var time: String? = null
        var status: String? = null
        var special: String? = null
        var mobileId: String? = null
        var fav: String? = null
        var mobId: String? = null
        var visitor: String? = null
        var addId: String? = null
        var postdate: String? = null
        var add_visitor: String? = null
        var title: String? = null
        var phone: String? = null
        var videoImage: String? = null
        var abbr: String? = null
        var for_id: String? = null
        var thumbnail: String? = null
        override fun toString(): String {
            return "Ads(id=$id, folder=$folder, key=$key, image=$image, video=$video, shopCategorie=$shopCategorie, subcatId=$subcatId, position=$position, description=$description, url=$url, subFor=$subFor, time=$time, status=$status, special=$special, mobileId=$mobileId, fav=$fav, mobId=$mobId, visitor=$visitor," +
                    " addId=$addId, postdate=$postdate, add_visitor=$add_visitor, title=$title, phone=$phone, videoImage=$videoImage, abbr=$abbr, forId=$for_id, thumbnail=$thumbnail)"
        }


    }

    inner class Tab {

        var id: String? = null
        var subcatname: String? = null
        var title: String? = null
        var phone: String? = null
        var categeory: String? = null
        var time: String? = null
        var image: String? = null

    }

    inner class Country {
        var id: String? = null
        var abbr: String? = null
        var name: String? = null
        var currency: String? = null
        var flag: String? = null
    }

    inner class Directory {
        var id: String? = null
        var folder: String? = null
        var image: String? = null
        var flag: String? = null
        var video: String? = null
        var phone: String? = null
        var email: String? = null
        var WhatsApp: String? = null
        var Name: String? = null
        var description: String? = null
        var tag: String? = null
        var abbr: String? = null
        var for_id: String? = null
        var  country: String? = null
        val gallery: List<String>? = null
    }
}
