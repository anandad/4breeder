package com.breeders.fragments


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.activities.PlayVideoActivity
import com.breeders.activities.ShowImageActivity
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Request
import com.squareup.picasso.RequestHandler
import kotlinx.android.synthetic.main.fragment_directory_details.view.*
import org.json.JSONObject
import java.io.IOException


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DirectoryDetailsFragment : Fragment(), ServiceEvents {

    lateinit var directory: Models.Directory


    lateinit var fragView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_directory_details, container, false)

        MainActivity.leftIcon.setImageResource(R.drawable.ic_left)
        MainActivity.rightIcon.visibility = View.GONE
        MainActivity.homeTitle.text = "4BREEDERS"


        MainActivity.leftIcon.setOnClickListener {
            activity!!.onBackPressed()
        }
        val id = arguments!!.getString("id")

        getSingleDirectory(id)

        fragView.iv_call.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + directory.phone)
            startActivity(intent)
        }
        fragView.iv_mail.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("sms:" + directory.phone)
            startActivity(intent)
        }
        fragView.iv_web.setOnClickListener {

            //   val contact = "+00 9876543210" // use country code with your phone number

            val url = "https://api.whatsapp.com/send?phone=$directory.phone"
            try {
                val pm = context!!.getPackageManager()
                pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            } catch (e: PackageManager.NameNotFoundException) {
                Toast.makeText(activity, "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT)
                    .show()
                e.printStackTrace()
            }

        }

        return fragView;
    }

    @SuppressLint("HardwareIds")
    private fun setUI() {
        // ads = arguments!!.getSerializable("ads") as Models.Ads
        fragView.tv_ads_dec.text = directory.description
        fragView.tv_name.text = directory.Name
        fragView.tv_tag.text = directory.tag

        Picasso.get().load(directory.flag).into(fragView.iv_flag_image)

        Picasso.get()
            .load(directory.image)
            .into(fragView.iv_directory_image, object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    fragView.pb_loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    fragView.pb_loading.visibility = View.GONE
                    //               TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })


        fragView.iv_directory_image.setOnClickListener {
            val intent = Intent(activity, ShowImageActivity::class.java)
            intent.putExtra("imageUrl", directory.image)
            intent.putExtra("imageNumber", 0)
            intent.putExtra("imageSize", 4)
            startActivity(intent)
        }

//        val videoRequestHandler = VideoRequestHandler()
//
//        val picassoInstance = Picasso.Builder (context!!.applicationContext)
//            .addRequestHandler(videoRequestHandler)
//            .build();
//
//        picassoInstance.load(videoRequestHandler.SCHEME_VIDEO+":"+directory.video)
//            .placeholder(R.color.colorPrimary)
//            .into(fragView.iv_video)

        if (directory.video != null && directory.video!!.isNotEmpty() && directory.video != "http://4breeders.com/attachments/shop_images/" ) {
            fragView.ll_video.visibility = View.VISIBLE
            fragView.iv_video.setOnClickListener {
                val intent = Intent(activity, PlayVideoActivity::class.java)
                intent.putExtra("videoUrl", directory.video)
                startActivity(intent)
            }
        }

        if (directory.gallery != null && directory.gallery!!.isNotEmpty()) {
            for (position in 0 until directory.gallery!!.size) {
                when (position) {
                    0 -> {
                        fragView.ll_img_one.visibility = View.VISIBLE
                        Picasso.get().load(directory.gallery!![position]).into(fragView.iv_one)
                        fragView.iv_one.setOnClickListener {
                            val intent = Intent(activity, ShowImageActivity::class.java)
                            intent.putExtra("imageUrl", directory.gallery!![0])
                            intent.putExtra("imageNumber", position)
                            intent.putExtra("imageSize", directory.gallery!!.size)
                            startActivity(intent)
                        }
                    }
                    1 -> {
                        fragView.ll_img_two.visibility = View.VISIBLE
                        Picasso.get().load(directory.gallery!![position]).into(fragView.iv_two)
                        fragView.iv_two.setOnClickListener {
                            val intent = Intent(activity, ShowImageActivity::class.java)
                            intent.putExtra("imageUrl", directory.gallery!![1])
                            intent.putExtra("imageNumber", position)
                            intent.putExtra("imageSize", directory.gallery!!.size)
                            startActivity(intent)
                        }
                    }
                    2 -> {
                        fragView.ll_img_three.visibility = View.VISIBLE
                        Picasso.get().load(directory.gallery!![position]).into(fragView.iv_three)
                        fragView.iv_three.setOnClickListener {
                            val intent = Intent(activity, ShowImageActivity::class.java)
                            intent.putExtra("imageUrl", directory.gallery!![2])
                            intent.putExtra("imageNumber", position)
                            intent.putExtra("imageSize", directory.gallery!!.size)
                            startActivity(intent)
                        }
                    }
                    3 -> {
                        fragView.ll_img_four.visibility = View.VISIBLE
                        Picasso.get().load(directory.gallery!![position]).into(fragView.iv_four)
                        fragView.iv_four.setOnClickListener {
                            val intent = Intent(activity, ShowImageActivity::class.java)
                            intent.putExtra("imageUrl", directory.gallery!![3])
                            intent.putExtra("imageNumber", position)
                            intent.putExtra("imageSize", directory.gallery!!.size)
                            startActivity(intent)
                        }
                    }
                }
            }
        }

    }

    private fun getParams(id: String?): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        params["id"] = id!!
        return params
    }

    private fun getSingleDirectory(id: String?) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.getSingledirectory, getParams(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            if (methodName.contains(Constant.getSingledirectory)) {
                directory =
                        Gson().fromJson(jsonObject.optJSONObject("data").toString(), Models.Directory::class.java)
                setUI()
            }
        } else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }

    inner class VideoRequestHandler : RequestHandler() {
        var SCHEME_VIDEO = "video"
        override fun canHandleRequest(data: Request): Boolean {
            val scheme = data.uri.getScheme()
            return SCHEME_VIDEO == scheme
        }

        @Throws(IOException::class)
        override fun load(data: Request, arg1: Int): RequestHandler.Result {
            val bm = ThumbnailUtils.createVideoThumbnail(data.uri.path, MediaStore.Images.Thumbnails.MINI_KIND)
            return RequestHandler.Result(bm, LoadedFrom.DISK)
        }
    }

}
