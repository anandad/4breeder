package com.breeders.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R
import com.breeders.models.Models
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_country.view.*
import android.widget.TextView
import android.widget.Filter


class CountryAdaptor(var context: Context, var countries: ArrayList<Models.Country>) : BaseAdapter(){

    var filteredCountry = countries

    override fun getCount(): Int {
        return filteredCountry.size
    }

    override fun getItem(i: Int): Any? {
        return filteredCountry[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_country,null)
        itemView.tv_con_name.text = filteredCountry[i].name!!.toUpperCase()
        Picasso.get()
            .load(filteredCountry[i].flag)
            .into(itemView.iv_con_image)
        return itemView
    }


    fun filter(countriess: ArrayList<Models.Country>, constraint: CharSequence?): ArrayList<Models.Country>{

        val filterCountry = ArrayList<Models.Country>()

        for (country in countries) {

            if (country.name!!.toLowerCase().contains(constraint.toString().toLowerCase())) {
                filterCountry.add(country);
            }
        }

        filteredCountry = filterCountry
        notifyDataSetChanged();

        return filterCountry;
    }
}
