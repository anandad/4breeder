package com.breeders.fragments


import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_aboutus.view.*
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */

class SupportFragment : Fragment(), ServiceEvents {


    lateinit var fragView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_aboutus, container, false)

        MainActivity.leftIcon.setImageResource(R.drawable.ic_left)
        MainActivity.rightIcon.visibility = View.GONE
        MainActivity.homeTitle.text  = getString(R.string.customer_support)


        MainActivity.leftIcon.setOnClickListener {
            activity!!.onBackPressed()
        }
        getAbout()
        return fragView;
    }

    private fun getLangParams(): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        return params
    }

    private fun getAbout() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.livechat, getLangParams(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {


        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {

            fragView.tv_title.loadData("<p align='right'>"+
                    jsonObject.optJSONArray("data").optJSONObject(0).optString("description").toString().replace("\n","</br>")
                    +"</p>"
                , "text/html", "utf-8"
            )
        }

        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {


    }


}
