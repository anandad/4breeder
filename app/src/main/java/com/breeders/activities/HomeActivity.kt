package com.breeders.activities

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.adaptors.CountryAdaptor
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.country_search.view.*
import org.json.JSONObject
import java.util.*


class HomeActivity : AppCompatActivity(), View.OnClickListener, ServiceEvents {


    // private var countries = ArrayList<String>()
    private var countries = ArrayList<Models.Country>()

    private lateinit var countryAdaptor: CountryAdaptor;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        UserPrefs.setSharedPreferences(this, "country", "")
        UserPrefs.setSharedPreferences(this, "language", "")
        getCountry()
        // sp_country.onItemSelectedListener = this;


        rl_country.setOnClickListener(this)
        ll_kuwait.setOnClickListener(this)
        ll_usa.setOnClickListener(this)
        ll_russia.setOnClickListener(this)
    }

    override fun onClick(p: View?) {

        when (p!!.id) {
            R.id.ll_russia -> {
                ll_russia.setBackgroundResource(R.drawable.back_circle)
                ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(this, "language", "ru")
                setLocale("ru")
                getLiterals()
            }
            R.id.ll_usa -> {
                ll_usa.setBackgroundResource(R.drawable.back_circle)
                ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))
                ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(this, "language", "en")
                setLocale("en")
                getLiterals()
            }
            R.id.ll_kuwait -> {
                ll_kuwait.setBackgroundResource(R.drawable.back_circle)
                ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(this, "language", "ar")
                setLocale("ar")
                getLiterals()
            }
            R.id.rl_country -> {
                showMessage()
            }
        }

    }

//    override fun onNothingSelected(p0: AdapterView<*>?) {
//
//    }

//    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//        Log.e("HomeActivity", " Country = " + p0!!.getItemAtPosition(p2).toString())
//        if (p2 != 0) {
//            UserPrefs.setSharedPreferences(this, "country", p0!!.getItemAtPosition(p2).toString())
//            UserPrefs.setSharedPreferences(this, "countryCode", countriesCode[p2])
//        }
//        if ((UserPrefs.getSharedPreferences(this, "country").isNotEmpty()) &&
//            (UserPrefs.getSharedPreferences(this, "language").isNotEmpty())
//        ) {
//            startActivity(Intent(this, MainActivity::class.java))
//            finish()
//        }
//    }



    private fun getCountry() {

        if (Utils.isOnline(this)) {
            val bzzzLoginServices = GetServices(this, this)
            try {
                bzzzLoginServices.RestCallAsync(Constant.getCountry, null, true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                this, "",
                getString(R.string.no_network)
            )
    }

    private fun getLangParams(): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(this, "language")
        return params
    }

    private fun getLiterals() {

        if (Utils.isOnline(this)) {
            val bzzzLoginServices = GetServices(this, this)
            try {
                bzzzLoginServices.RestCallAsync(Constant.options, getLangParams(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                this, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {

            if (methodName.contains(Constant.getCountry)) {

                countries.clear()
                //        countriesCode.clear()

                //       countries.add(getString(R.string.select_country))
                //       countriesCode.add("+0")

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    countries.add(
                        Gson().fromJson(
                            dataArray.optJSONObject(i).toString(),
                            Models.Country::class.java
                        )
                    )
                    //           countriesCode.add(dataArray.optJSONObject(i).optString("abbr"))
                }

                countries.sortBy({ selector(it) })

                // Create an ArrayAdapter using the string array and a default spinner layout
                countryAdaptor = CountryAdaptor(this, countries)

            } else if (methodName.contains(Constant.options)) {
                UserPrefs.setSharedPreferences(
                    this, "literals",
                    jsonObject.optJSONArray("data").optJSONObject(0).toString()
                )

                if ((UserPrefs.getSharedPreferences(this, "country").isNotEmpty()) &&
                    (UserPrefs.getSharedPreferences(this, "language").isNotEmpty())
                ) {
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        } else {
            Utils.showDialog(
                this, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            this, "", error!!
        )
    }

    override fun EndedRequest() {

    }

    fun selector(con: Models.Country): String? = con.name

    private fun setLocale(localeName: String) {
        val myLocale = Locale(localeName)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
//            val refresh = Intent(this, MainActivity::class.java)
//            refresh.putExtra(currentLang, localeName)
//            startActivity(refresh)
    }

    private val handleAppCrash = Thread.UncaughtExceptionHandler { thread, ex ->
        Log.e("error", ex.toString())
        //send email here
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("ak79382@gamil.com"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Crash Report")
        i.putExtra(Intent.EXTRA_TEXT, ex.toString())
        try {
            startActivity(Intent.createChooser(i, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }

    }

    private fun setUI(country: Models.Country) {
        tv_con_name.text = country.name!!.toUpperCase()
        Picasso.get()
            .load(country.flag)
            .placeholder(R.mipmap.usa_flag)
            .into(iv_con_image)

        UserPrefs.setSharedPreferences(this, "country", country.name)
        UserPrefs.setSharedPreferences(this, "countryCode", country.abbr)
        UserPrefs.setSharedPreferences(this, "countryFlag", country.flag)

        if ((UserPrefs.getSharedPreferences(this, "country").isNotEmpty()) &&
            (UserPrefs.getSharedPreferences(this, "language").isNotEmpty())
        ) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun showMessage() {

        val dialog = Dialog(this)
        val view = LayoutInflater.from(this).inflate(
            R.layout.country_search, null
        )

        view.lv_country_list.adapter = countryAdaptor

        view.lv_country_list.setOnItemClickListener { parent, view, position, id ->
            dialog.dismiss()
            setUI(countries[position])
        }

        view.et_search_text.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                println("Text [$s]")
                countries = countryAdaptor.filter(countries, s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {

            }

            override fun afterTextChanged(s: Editable) {}
        })

        //     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //      Objects.requireNonNull<Window>(dialog.window).setBackgroundDrawableResource(R.color.colorTransparent)
        dialog.setContentView(view)
        dialog.setCancelable(true)
        dialog.show()
    }

}
