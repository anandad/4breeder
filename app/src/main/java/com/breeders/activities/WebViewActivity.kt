package com.breeders.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.breeders.R
import kotlinx.android.synthetic.main.activity_web_view.*
import android.webkit.WebView
import android.webkit.WebViewClient


class WebViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        wv_links.settings.javaScriptEnabled = true
        wv_links.loadUrl(intent.getStringExtra("url"))

// you need to setWebViewClient for forcefully open in your webview
        wv_links.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        iv_back.setOnClickListener {
            finish()
        }

    }
}
