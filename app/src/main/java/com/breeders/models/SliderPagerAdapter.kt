package com.breeders.models

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.breeders.R
import com.breeders.activities.WebViewActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_detail.view.*
import java.lang.Exception

class SliderPagerAdapter(fm: FragmentManager, var data: ArrayList<String>, var URL: ArrayList<String>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return PlaceholderFragment.newInstance(position, data[position],URL[position])
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return data.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}

class PlaceholderFragment : Fragment() {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    private var url: String? = ""
    private var image: String? = ""

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        this.url = args!!.getString(ARG_IMG_URL)
        this.image = args!!.getString(ARG_IMG)
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_detail, container, false)

        Picasso.get()
            .load(image)
            .resize(400, 300)
            .into(rootView.detail_image,  object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    rootView.pb_loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    rootView.pb_loading.visibility = View.GONE
    //                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })

        rootView.setOnClickListener {
//            val i = Intent(Intent.ACTION_VIEW)
//            i.data = Uri.parse(url)

            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra("url", url)
            startActivity(intent)


        }

        return rootView
    }

    companion object {
        private val ARG_IMG_URL = "image"
        private val ARG_IMG = "image_url"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int, image: String, url: String): PlaceholderFragment {
            val fragment = PlaceholderFragment()
            val args = Bundle()
            args.putString(ARG_IMG, image)
            args.putString(ARG_IMG_URL, url)
            fragment.arguments = args
            return fragment
        }
    }

}