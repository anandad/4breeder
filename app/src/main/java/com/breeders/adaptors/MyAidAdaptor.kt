package com.breeders.adaptors

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R
import com.breeders.fragments.AddBidFragment
import com.breeders.models.Models
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_my_aid.view.*

class MyAidAdaptor internal constructor(var context: Context, var adsList: ArrayList<Models.Ads>) : BaseAdapter() {

    override fun getCount(): Int {
        return adsList.size
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val itemView =   LayoutInflater.from(context).inflate(R.layout.item_my_aid,null)

        itemView.tv_ads_des.text = adsList[i].description

        Picasso.get()
            .load(adsList[i].image)
            .into(itemView.iv_ads_image,  object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    itemView.pb_loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    itemView.pb_loading.visibility = View.GONE
   //                 TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })

        itemView.rl_edit.setOnClickListener {
            val bidDetailsFragment = AddBidFragment()
            val argument = Bundle()
            argument.putSerializable("ads", adsList[i])
            bidDetailsFragment.arguments = argument
            (context as AppCompatActivity).supportFragmentManager !!.beginTransaction().addToBackStack(null)
                .replace(R.id.frag_container, bidDetailsFragment).commit()
        }

        return itemView
    }
}
