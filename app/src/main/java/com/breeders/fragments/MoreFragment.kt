package com.breeders.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.activities.HomeActivity
import kotlinx.android.synthetic.main.fragment_more.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class MoreFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val fragView =  inflater.inflate(R.layout.fragment_more, container, false)

        Constant.setFlag(context!!)

        fragView.ll_my_aid.setOnClickListener(this)
        fragView.ll_favourite.setOnClickListener(this)
        fragView.ll_setting.setOnClickListener(this)
        fragView.ll_about_us.setOnClickListener(this)
        fragView.ll_contact.setOnClickListener(this)
        fragView.ll_support.setOnClickListener(this)
        return fragView;
    }

    override fun onClick(p: View?) {

        when (p!!.id) {
            R.id.ll_my_aid ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, MyAidFragment()).commit()
            R.id.ll_favourite ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, FavouriteFragment()).commit()
            R.id.ll_setting ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, SettingFragment()).commit()
            R.id.ll_about_us ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, AboutUsFragment()).commit()
            R.id.ll_contact ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, ContactFragment()).commit()
            R.id.ll_support ->
                fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, SupportFragment()).commit()
        }
    }


}
