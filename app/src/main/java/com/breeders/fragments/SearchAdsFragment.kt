package com.breeders.fragments


import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.adaptors.AdsAdaptor
import com.breeders.models.Models
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.json.JSONObject


class SearchAdsFragment : Fragment(), ServiceEvents {

    private var adsList = ArrayList<Models.Ads>()
    private var tabs = ArrayList<Models.Tab>()
    lateinit var adsAdaptor: AdsAdaptor
    lateinit var fragView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        fragView = inflater.inflate(R.layout.fragment_search, container, false)

        Constant.setBack(activity!!, getString(R.string.search_menu_title))

        adsAdaptor = AdsAdaptor(context!!, adsList)

        fragView.gv_subcat_list.setOnItemClickListener { adapterView, view, i, l ->
            val adsDetailFragment = AidDetailsFragment()
            val argument = Bundle()
            argument.putSerializable("ads", adsList[i])
            adsDetailFragment.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, adsDetailFragment)
                .commit()
        }

        //in your Activity or Fragment where of Adapter is instantiated :

        fragView.et_search_text.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                println("Text [$s]")

                val filteredAdsList = adsAdaptor.filter(s.toString(), adsList)

                adsAdaptor = AdsAdaptor(context!!, filteredAdsList)

                fragView.gv_subcat_list.adapter = adsAdaptor
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {

            }

            override fun afterTextChanged(s: Editable) {}
        })

        getAds()

        return fragView;
    }

    private fun getLangParam(): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        return param
    }

    private fun getAds() {
        val bzzzLoginServices = GetServices(this, context)
        try {
            bzzzLoginServices.RestCallAsync(Constant.getAds, getLangParam(), true)
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {


        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {


            when {
                methodName.contains(Constant.getAds) -> {
                    adsList.clear()
                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        adsList.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Ads::class.java))
                    }

                }

            }
        }

        else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }

}
