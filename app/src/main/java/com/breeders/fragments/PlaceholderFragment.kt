package com.breeders.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.breeders.R
import com.breeders.models.PlaceholderFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_ads_slider.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PlaceholderFragment : Fragment() {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    private var url: String? = ""

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        this.url = args!!.getString(ARG_IMG_URL)
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_ads_slider, container, false)

        Picasso.get()
            .load(url)
            .resize(400, 300)
            .into(rootView.iv_image,  object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    rootView.pb_loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    rootView.pb_loading.visibility = View.GONE
   //                 TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })

        return rootView
    }

    companion object {
        private val ARG_IMG_URL = "image_url"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int, url: String): PlaceholderFragment {
            val fragment = PlaceholderFragment()
            val args = Bundle()
            args.putString(ARG_IMG_URL, url)
            fragment.arguments = args
            return fragment
        }
    }

}
