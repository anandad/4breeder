package com.breeders.adaptors

import android.content.Context
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R
import com.breeders.models.Models
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_bid_list.view.*
import java.text.SimpleDateFormat
import java.util.*


class BidsAdaptor internal constructor(var context: Context, var bids: ArrayList<Models.Bid>) : BaseAdapter() {

    override fun getCount(): Int {
        return bids.size
    }

    override fun getItem(i: Int): Any? {
        return bids[i]
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_bid_list,null)
        itemView.tv_bid_name.text = bids[i].bid_name
        itemView.tv_bid_dec.text = bids[i].bid_type
        Picasso.get()
            .load(bids[i].image)
            .resize(400, 300)
            .into(itemView.iv_bid_image, object : com.squareup.picasso.Callback {
                override fun onSuccess() {
                    itemView.pb_loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    itemView.pb_loading.visibility = View.GONE
          //          TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })

        val format = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH)


        val date = format.parse(bids[i].Day+" "+bids[i].Start_Time)

        val last = date.time

        val current = Date().time
//
//        Log.e("BidDetail"," Last Date => "+date)
//        Log.e("BidDetail"," Current Date => "+ Date())


        object : CountDownTimer(last - current, 1000) {

            override fun onTick(diff: Long) {
                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000)
                itemView.tv_count_down.text = "$diffHours:$diffMinutes:$diffSeconds"
            }

            override fun onFinish() {
                //   mTextField.setText("done!")
            }
        }.start()

        return itemView
    }


}
