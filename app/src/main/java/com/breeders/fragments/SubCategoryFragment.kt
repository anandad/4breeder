package com.breeders.fragments


import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TabHost
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.adaptors.FavouriteAdaptor
import kotlinx.android.synthetic.main.fragment_sub_category.view.*
import android.widget.TextView
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.adaptors.AdsAdaptor
import com.breeders.adaptors.HomeAdaptor
import com.breeders.models.Models
import com.google.gson.Gson
import org.json.JSONObject


class SubCategoryFragment : Fragment(), ServiceEvents {

    private var adsList = ArrayList<Models.Ads>()
    private var specialAdsList = ArrayList<Models.Ads>()
    private var tabs = ArrayList<Models.Tab>()
    lateinit var adsAdaptor: AdsAdaptor
    lateinit var fragView: View
    lateinit var categoryId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        fragView = inflater.inflate(R.layout.fragment_sub_category, container, false)

        Constant.setBack(activity!!,"")

        categoryId = arguments!!.getString("id")!!



        adsAdaptor = AdsAdaptor(context!!, adsList)

        fragView.gv_subcat_list.adapter = adsAdaptor

        fragView.gv_subcat_list.setOnItemClickListener { adapterView, view, i, l ->
            val adsDetailFragment = AidDetailsFragment()
            val argument = Bundle()
            argument.putSerializable("ads", adsList[i])
            adsDetailFragment.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null).replace(R.id.frag_container, adsDetailFragment)
                .commit()
        }

        fragView.sub_cat_tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{

            override fun onTabReselected(p0: TabLayout.Tab?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> getAllAds(categoryId)
                    else -> getSubCatAds(tabs[(tab.position) - 1].id)
                }
            }
        })

        getTabs(categoryId)
        return fragView;
    }

    fun getTabView(tabName: String?): View {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        val v = LayoutInflater.from(context).inflate(R.layout.custom_sub_cat_tab, null)
        val tv = v.findViewById(R.id.tv_title) as TextView
        tv.text = tabName
        return v
    }

    private fun getLangParam(id: String): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("id", id)
        return param
    }


    private fun getTabs(id: String) {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.categorySubList, getAdsParam(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getAdsParam(id: String?): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("country", UserPrefs.getSharedPreferences(context, "countryCode"))
        param.set("shop_categorie", id!!)
        return param
    }

    private fun getFilterAdsParam(id: String?): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("country", UserPrefs.getSharedPreferences(context, "countryCode"))
        param.set("subcat_id", id!!)
        return param
    }

    private fun getAllAds(id: String) {
        val bzzzLoginServices = GetServices(this, context)
        try {
            bzzzLoginServices.RestCallAsync(Constant.categoryAds, getAdsParam(id),true)
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }

    private fun getSubCatAds(id: String?) {
        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.subCategoryAds, getFilterAdsParam(id), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            when {
                methodName.contains(Constant.subCategoryAds) -> {
                    var tempAds = ArrayList<Models.Ads>()
                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        tempAds.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Ads::class.java))
                    }
                    var specialAds: List<Models.Ads> = tempAds.filter { ads -> ads.special == "S" }
                    var normalAds: List<Models.Ads> = tempAds.filter { ads -> ads.special == null }

                    adsList.clear()

                    adsList.addAll(specialAds)
                    adsList.addAll(normalAds)

                    adsAdaptor.notifyDataSetChanged()
                }
                methodName.contains(Constant.categorySubList) -> {
                    tabs.clear()

                    val dataArray = jsonObject.optJSONArray("data")
                    fragView.sub_cat_tab.addTab(fragView.sub_cat_tab.newTab().setCustomView(getTabView(getString(R.string.all))))
                    for (i in 0 until dataArray.length()) {
                        tabs.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Tab::class.java))
                        fragView.sub_cat_tab.addTab(fragView.sub_cat_tab.newTab().setCustomView(getTabView(tabs[i].title)))
                    }
             //       getAds()
                }
                methodName.contains(Constant.categoryAds) -> {
                    val tempAds = ArrayList<Models.Ads>()
                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        tempAds.add(Gson().fromJson(dataArray.optJSONObject(i).toString(), Models.Ads::class.java))
                    }
                    val specialAds: List<Models.Ads> = tempAds.filter { ads -> ads.special == "S" }
                    val normalAds: List<Models.Ads> = tempAds.filter { ads -> ads.special == null }

                    adsList.clear()

                    adsList.addAll(specialAds)
                    adsList.addAll(normalAds)


                    adsAdaptor.notifyDataSetChanged()

                }
            }
        }
        else if(methodName.contains(Constant.subCategoryAds)){
            adsList.clear()

            adsAdaptor.notifyDataSetChanged()
        }
//        else {
//            Utils.showDialog(
//                context!!, "",
//                jsonObject.optString("message")
//            )
//        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }

}
