package com.breeders.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.Window
import android.widget.ArrayAdapter
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import kotlinx.android.synthetic.main.activity_home.*
import org.json.JSONObject
import java.util.*


class SplashActivity : Activity(){


    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash)

        val android_id = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        );
        UserPrefs.setSharedPreferences(this, "mobile_id", android_id)
        val handler = Handler()
        handler.postDelayed({
            if ((UserPrefs.getSharedPreferences(this, "country").isNotEmpty()) &&
                (UserPrefs.getSharedPreferences(this, "language").isNotEmpty())
            ) {
                setLocale(UserPrefs.getSharedPreferences(this, "language"))
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
            }
        }, 3000)
    }

    private fun setLocale(localeName: String) {
        val myLocale = Locale(localeName)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
//            val refresh = Intent(this, MainActivity::class.java)
//            refresh.putExtra(currentLang, localeName)
//            startActivity(refresh)
    }

}
