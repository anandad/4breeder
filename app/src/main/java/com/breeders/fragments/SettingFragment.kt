package com.breeders.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.MainActivity
import com.breeders.adaptors.CountryAdaptor
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.country_search.view.*
import kotlinx.android.synthetic.main.fragment_setting.view.*
import org.json.JSONObject
import java.util.*


class SettingFragment : Fragment(), View.OnClickListener, ServiceEvents {

    private var countries = ArrayList<Models.Country>()

    private lateinit var countryAdaptor: CountryAdaptor;

    lateinit var fragView: View
    lateinit var adapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_setting, container, false)


        Constant.setBack(activity!!, getString(R.string.setting))

        fragView.rl_country.setOnClickListener(this)

        fragView.ll_kuwait.setOnClickListener(this)
        fragView.ll_usa.setOnClickListener(this)
        fragView.ll_russia.setOnClickListener(this)
        setUI()
        getCountry()

        return fragView;
    }

    override fun onClick(p: View?) {

        when (p!!.id) {
            R.id.ll_russia -> {
                setLocale("ru")
                fragView.ll_russia.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(context, "language", "ru")
            }
            R.id.ll_usa -> {
                setLocale("en")
                fragView.ll_usa.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(context, "language", "en")
            }
            R.id.ll_kuwait -> {
                setLocale("ar")
                fragView.ll_kuwait.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))
                UserPrefs.setSharedPreferences(context, "language", "ar")
            }
            R.id.rl_country -> {
                showMessage()
            }
        }

        if ((UserPrefs.getSharedPreferences(context, "country").isNotEmpty()) &&
            (UserPrefs.getSharedPreferences(context, "language").isNotEmpty()) &&
            p.id != R.id.rl_country
        ) {
            getLiterals()
        }

    }


    private fun getLangParams(): Map<String, String> {
        val params = HashMap<String, String>()
        params["lang"] = UserPrefs.getSharedPreferences(context, "language")
        return params
    }

    private fun getLiterals() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.options, getLangParams(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getCountry() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.getCountry, null, true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {

            if (methodName.contains(Constant.getCountry)) {

                countries.clear()
                //        countriesCode.clear()

                //       countries.add(getString(R.string.select_country))
                //       countriesCode.add("+0")

                val dataArray = jsonObject.optJSONArray("data")

                for (i in 0 until dataArray.length()) {
                    countries.add(
                        Gson().fromJson(
                            dataArray.optJSONObject(i).toString(),
                            Models.Country::class.java
                        )
                    )
                    //           countriesCode.add(dataArray.optJSONObject(i).optString("abbr"))
                }

                countries.sortBy({ selector(it) })

                // Create an ArrayAdapter using the string array and a default spinner layout
                countryAdaptor = CountryAdaptor(context!!, countries)

            } else if (methodName.contains(Constant.options)) {
                UserPrefs.setSharedPreferences(
                    context, "literals",
                    jsonObject.optJSONArray("data").optJSONObject(0).toString()
                )

                if ((UserPrefs.getSharedPreferences(context, "country").isNotEmpty()) &&
                    (UserPrefs.getSharedPreferences(context, "language").isNotEmpty())
                ) {
                    startActivity(Intent(activity, MainActivity::class.java))
                    activity!!.finish()
                }
            }
        } else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {


    }

    fun selector(con: Models.Country): String? = con.name


    @SuppressLint("DefaultLocale")
    private fun setUI() {

        when (UserPrefs.getSharedPreferences(context, "language")) {
            "ru" -> {
                fragView.ll_russia.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))

            }
            "en" -> {
                fragView.ll_usa.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_kuwait.setBackgroundColor(resources.getColor(android.R.color.transparent))

            }
            "ar" -> {
                fragView.ll_kuwait.setBackgroundResource(R.drawable.back_circle)
                fragView.ll_usa.setBackgroundColor(resources.getColor(android.R.color.transparent))
                fragView.ll_russia.setBackgroundColor(resources.getColor(android.R.color.transparent))

            }
        }
        fragView.tv_con_name.text = UserPrefs.getSharedPreferences(context, "country").toUpperCase()
        Picasso.get()
            .load(UserPrefs.getSharedPreferences(context, "countryFlag"))
            .into(fragView.iv_con_image)

    }

    private fun setLocale(localeName: String) {
        val myLocale = Locale(localeName)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
//            val refresh = Intent(this, MainActivity::class.java)
//            refresh.putExtra(currentLang, localeName)
//            startActivity(refresh)
    }

    private fun setUI(country: Models.Country) {

        fragView.tv_con_name.text = country.name!!.toUpperCase()
        Picasso.get()
            .load(country.flag)
            .into(fragView.iv_con_image)

        UserPrefs.setSharedPreferences(context, "country", country.name)
        UserPrefs.setSharedPreferences(context, "countryFlag", country.flag)
        UserPrefs.setSharedPreferences(context, "countryCode", country.abbr)

        if ((UserPrefs.getSharedPreferences(context, "country").isNotEmpty()) &&
            (UserPrefs.getSharedPreferences(context, "language").isNotEmpty())
        ) {
            startActivity(Intent(activity, MainActivity::class.java))
            activity!!.finish()
        }

    }

    private fun showMessage() {

        val dialog = Dialog(context)
        val view = LayoutInflater.from(context).inflate(
            R.layout.country_search, null
        )

        view.lv_country_list.adapter = countryAdaptor

        view.lv_country_list.setOnItemClickListener { parent, view, position, id ->
            dialog.dismiss()
            setUI(countries[position])
        }

        view.et_search_text.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                println("Text [$s]")
                countries = countryAdaptor.filter(countries, s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {

            }

            override fun afterTextChanged(s: Editable) {}
        })

        //     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        //      Objects.requireNonNull<Window>(dialog.window).setBackgroundDrawableResource(R.color.colorTransparent)
        dialog.setContentView(view)
        dialog.setCancelable(true)
        dialog.show()
    }

}