package com.breeders.models

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.GridView
import android.view.ViewGroup



class GridViewScrollable(context: Context?) : GridView(context) {

    private var expanded = false

    fun GridViewScrollable(context: Context){
     //   super(context)
    }

    fun GridViewScrollable(context: Context, attrs: AttributeSet){
     //   super(context, attrs)
    }

    fun GridViewScrollable(
        context: Context, attrs: AttributeSet,
        defStyle: Int
    ) {
    //    super(context, attrs, defStyle)
    }

    fun isExpanded(): Boolean {
        return expanded
    }

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // HACK! TAKE THAT ANDROID!
        if (isExpanded()) {
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            val expandSpec = View.MeasureSpec.makeMeasureSpec(
                View.MEASURED_SIZE_MASK,
                View.MeasureSpec.AT_MOST
            )
            super.onMeasure(widthMeasureSpec, expandSpec)

            val params = layoutParams
            params.height = measuredHeight
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }

    fun setExpanded(expanded: Boolean) {
        this.expanded = expanded
    }
}
