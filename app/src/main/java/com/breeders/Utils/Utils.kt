package com.adandroid.mobileinfo.utils

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.ContentUris
import android.content.Context
import android.content.DialogInterface.OnClickListener
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.graphics.Bitmap.Config
import android.graphics.BitmapFactory.Options
import android.graphics.Paint.Style
import android.graphics.PorterDuff.Mode
import android.graphics.drawable.GradientDrawable
import android.location.LocationManager
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Environment
import android.os.Vibrator
import android.provider.CallLog
import android.provider.ContactsContract
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.telephony.SmsManager
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.text.format.Formatter
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import android.widget.EditText
import android.widget.ImageView
import android.widget.ListView
import android.widget.Toast
import com.breeders.R
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.net.URL
import java.nio.channels.FileChannel
import java.sql.Time
import java.text.Format
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList


/**
 * @author admin
 */
/**
 * @author admin
 */

/**
 * @author admin
 */
@SuppressLint("NewApi")
class Utils {

    fun <E> writeObjectIntoFile(e: E, fileName: String, context: Context) {
        try {
            val fos = context.openFileOutput(fileName,
                    Context.MODE_PRIVATE)
            val os = ObjectOutputStream(fos)

            os.writeObject(e)
            os.close()
        } catch (exc: Exception) {
            exc.printStackTrace()
        }

    }


    fun <E> readObjectIntoFile(fileName: String, context: Context): E? {
        try {
            val fis = context.openFileInput(fileName)
            val `is` = ObjectInputStream(fis)
            val e = `is`.readObject() as E
            `is`.close()

            return e
        } catch (exc: Exception) {
            exc.printStackTrace()
        }

        return null
    }


    companion object {

        @JvmStatic
        fun showDialog(ctx: Context, title: String, msg: String,
                       btn1: String, btn2: String?,
                       listener1: OnClickListener,
                       listener2: OnClickListener?): AlertDialog {

            val builder = AlertDialog.Builder(ctx)
            builder.setTitle(title)
            builder.setMessage(msg).setCancelable(false)
                    .setPositiveButton(btn1, listener1)
            if (btn2 != null)
                builder.setNegativeButton(btn2, listener2)

            val alert = builder.create()
            alert.show()
            return alert

        }

        @JvmStatic
        fun getValidMobileNo(mobileNo: String): String {
            var mobileNo = mobileNo

            if (mobileNo.length > 10) {
                if (hasCountryCode(mobileNo)) {
                    // +52 for MEX +526441122345, 13-10 = 3, so we need to remove 3 characters
                    val country_digits = mobileNo.length - 10
                    mobileNo = mobileNo.substring(country_digits)
                }

                if (mobileNo[0].toString().equals("0")) {
                    mobileNo = mobileNo.substring(1)
                }

            }
            return mobileNo
        }


        @JvmStatic
        fun hasCountryCode(number: String): Boolean {
            return number[0] == '+'
        }

        @JvmStatic
        fun convertDpToPixels(context: Context, dp: Float): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return dp * (metrics.densityDpi / 160f)
        }


        @JvmStatic
        val latestDateTime: String
            get() {

                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR).toString()
                val month = c.get(Calendar.MONTH).toString()
                val date = c.get(Calendar.DATE).toString()
                val time = (c.get(Calendar.HOUR).toString() + ":"
                        + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND))

                return "$date-$month-$year $time"
            }

        // get diffrence between to date in min
        @JvmStatic
        fun getDateDifference(startDate: String, endDate: String): Long {

            val sdf = SimpleDateFormat("yyyy-mm-dd h:mm:ss", Locale.ENGLISH)
            var date1: Date? = null
            var date2: Date? = null
            try {
                date1 = sdf.parse(startDate)
                date2 = sdf.parse(endDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val diff = date2!!.time - date1!!.time
            return diff / 1000
        }

        @JvmStatic
        fun showDialog(ctx: Context, title: String, msg: String,
                       btn1: String, btn2: String, listener: OnClickListener): AlertDialog {

            return showDialog(ctx, title, msg, btn1, btn2, listener,
                    OnClickListener { dialog, id -> dialog.dismiss() })

        }

        @JvmOverloads
        @JvmStatic
        fun showDialog(ctx: Context, title: String, msg: String, btn: String,
                       listener: OnClickListener = OnClickListener { dialog, id -> dialog.dismiss() }): AlertDialog {

            return showDialog(ctx, title, msg, btn, null, listener, null)
        }

        @JvmOverloads
        @JvmStatic
        fun showDialog(ctx: Context, title: String, msg: String,
                       listener: OnClickListener = OnClickListener { dialog, id -> dialog.dismiss() }): AlertDialog {

            return showDialog(ctx, title, msg, "Ok!", null, listener, null)
        }

        @JvmStatic
        fun showDialogf(ctx: Activity, title: String, msg: String): AlertDialog {

            return showDialog(ctx, title, msg,
                    OnClickListener { dialog, id ->
                        dialog.dismiss()
                        ctx.finish()
                    })

        }

        @JvmStatic
        fun showNoNetworkDialog(ctx: Context) {

            showDialog(ctx, "No Network Connection",
                    "Internet is not available. Please check your network connection.")
                    .show()
        }

        @JvmStatic
        fun splitStringByGivenChar(str: String, splitChar: String): String {

            val parts = str.split(splitChar.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            return parts[0]

        }


        @JvmStatic
        fun isOnline(context: Context): Boolean {

            val conMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            return (conMgr.activeNetworkInfo != null

                    && conMgr.activeNetworkInfo.isAvailable

                    && conMgr.activeNetworkInfo.isConnected)
        }

        fun isInternetAvailable(): Boolean {
            return try {
                val ipAddr = InetAddress.getByName("google.com");
                //You can replace it with your name
                !ipAddr.equals("");

            } catch (e: Exception) {
                false;
            }
        }

        @JvmStatic
        fun deleteDir(dir: File?): Boolean {
            if (dir != null && dir.isDirectory) {
                val children = dir.list()
                Log.d("file", dir.list().size.toString() + dir.absolutePath)
                for (i in children.indices) {
                    val success = deleteDir(File(dir, children[i]))
                    if (!success) {
                        return false
                    }
                }
            }
            // The directory is now empty so delete it
            return dir!!.delete()
        }

        @JvmStatic
        fun isValidEmail(email: String): Boolean {

            val emailExp = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,10}$"
            val pattern = Pattern.compile(emailExp, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            return matcher.matches()
        }

        @JvmStatic
        fun isNumeric(number: String): Boolean {

            val numExp = "^[-+]?[0-9]*\\.?[0-9]+$"
            val pattern = Pattern.compile(numExp, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(number)
            return matcher.matches()
        }

        @JvmStatic
        fun getDoubleFromString(number: String): String {

            val numExp = "^[-+]?[0-9]*\\.?[0-9]+$"
            val pattern = Pattern.compile(numExp, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(number)
            val match = matcher.matches()
            return if (match)
                number
            else
                "0.0"
        }

        @JvmStatic
        fun getIntFromString(number: String): Int {

            val numExp = ".*[^0-9].*"
            val pattern = Pattern.compile(numExp, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(number)
            val match = matcher.matches()
            return if (match)
                Integer.parseInt(number)
            else
                0
        }

        @JvmStatic
        fun hideKeyboard(ctx: Activity) {

            if (ctx.currentFocus != null) {
                val imm = ctx
                        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(ctx.currentFocus!!.windowToken,
                        0)
            }
        }

        @JvmStatic
        fun showKeyboard(ctx: Activity) {

            if (ctx.currentFocus != null) {
                val imm = ctx
                        .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }
        }

        @JvmStatic
        fun dialNumber(act: Activity, number: String) {

            val call = Intent(Intent.ACTION_DIAL)
            call.data = Uri.parse("tel:$number")
            act.startActivity(call)
        }

        @JvmStatic
        fun makeCall(act: Activity, number: String) {

            val num = number.replace(" ", "").replace("-", "")
            val digits = "0123456789"
            var phNum = ""
            for (i in 0 until num.length) {
                if (digits.contains(num[i] + "")) {
                    phNum += num[i]
                }
            }

            Log.d("phone", "$number=$phNum")

            showDialog(act, "Call", "Call $phNum", "Ok", "Cancel",
                    OnClickListener { dialog, which ->
                        val call = Intent(Intent.ACTION_CALL)
                        call.data = Uri.parse("tel:$phNum")
                        if (ActivityCompat.checkSelfPermission(act, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return@OnClickListener
                        }
                        act.startActivity(call)
                    }).show()
        }

        @JvmStatic
        fun sendMessage(act: Context, phone: String, msg: String) {
            try {

                val sms = SmsManager.getDefault()

                val messages = sms.divideMessage(msg)

                //   val recipient = recipientTextEdit.getText().toString()

                for (message in messages) {
                    sms.sendTextMessage(phone, null, message, PendingIntent.getBroadcast(
                            act, 0, Intent("ACTION_SMS_SENT"), 0), null)
                }

                Toast.makeText(act, "Message Sent to "+phone,
                        Toast.LENGTH_LONG).show()

            } catch (ex: Exception) {
                Toast.makeText(act, ex.message.toString(),
                        Toast.LENGTH_LONG).show()
                ex.printStackTrace()
            }


//            val num = phone.replace(" ", "").replace("-", "")
//            val digits = "0123456789"
//            phNum = ""
//            for (i in 0 until num.length) {
//                if (digits.contains(num[i] + "")) {
//                    phNum += num[i]
//                } else
//                    break
//            }
//
//            val msg = Intent(Intent.ACTION_SENDTO)
//            msg.data = Uri.parse("smsto:" + phone.trim { it <= ' ' })
//            act.startActivity(msg)

        }

        @JvmStatic
        fun sendEmail(act: Activity, email: String,
                      subject: String) {

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/html"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)

            act.startActivity(Intent.createChooser(intent, "Send Email"))
        }

        @JvmStatic
        fun copyFile(src: File, dst: File) {

            try {
                var outputChannel: FileChannel? = null
                var inputChannel: FileChannel? = null
                try {
                    outputChannel = FileOutputStream(dst).getChannel();
                    inputChannel = FileInputStream(src).getChannel();
                    inputChannel.transferTo(0, inputChannel.size(), outputChannel);
                    inputChannel.close();
                } finally {
                    if (inputChannel != null) inputChannel.close();
                    if (outputChannel != null) outputChannel.close();
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        @JvmStatic
        fun createNoMediaFile(dir: File) {

            try {
                val f = File(dir, ".nomedia")
                if (!f.exists())
                    f.createNewFile()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        @JvmStatic
        fun copyFileToByteArray(file: File) : ByteArray {
            val size = file.length().toInt();
            var bytes = ByteArray(size);
            try {
                val buf = BufferedInputStream(FileInputStream(file));
                buf.read(bytes, 0, bytes.size);
                buf.close();
            } catch (e: FileNotFoundException) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return bytes

        }

        @JvmStatic
        fun pad(c: Int): String {
            return if (c >= 10)
                c.toString()
            else
                "0$c"
        }

        @JvmStatic
        fun getTypeFace(ctx: Context): Typeface {
            return Typeface.createFromAsset(ctx.assets, "GOTHIC.TTF")
        }

        @JvmStatic
        fun getBoldTypeFace(ctx: Context): Typeface {
            return Typeface.createFromAsset(ctx.assets, "GOTHICB.TTF")
        }

        @JvmStatic
        fun getRealPathFromURI(contentUri: Uri, act: Activity): String? {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = act.managedQuery(contentUri, proj, null, null, null) ?: return null

            val column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        }

        @JvmStatic
        fun getRealPathForKitkatFromURI(contentUri: Uri, act: Activity): String {
            val wholeID = DocumentsContract.getDocumentId(contentUri)

            // Split at colon, use second item in the array
            val id = wholeID.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]

            val column = arrayOf(MediaStore.Images.Media.DATA)

            // where id is equal to
            val sel = MediaStore.Images.Media._ID + "=?"

            val cursor = act.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, arrayOf(id), null)

            val columnIndex = cursor!!.getColumnIndex(column[0])
            var path = ""
            if (cursor.moveToFirst()) {

                Log.d("cursor gallery", cursor.getString(columnIndex) + "")
                path = cursor.getString(columnIndex)
            }
            return path
        }

        @JvmStatic
        fun getOrientationFixedImage(f: File): Bitmap? {

            try {
                val exif = ExifInterface(f.path)
                val orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL)

                var angle = 0

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270
                }

                val mat = Matrix()
                mat.postRotate(angle.toFloat())

                val bmp = getCompressedBm(f)
                return Bitmap.createBitmap(bmp!!, 0, 0, bmp.width,
                        bmp.height, mat, true)
            } catch (e: IOException) {
                Log.w("TAG", "-- Error in setting image")
            } catch (oom: OutOfMemoryError) {
                Log.w("TAG", "-- OOM Error in setting image")
            }

            return getCompressedBm(f)
        }

        @JvmStatic
        fun getCompressedBm(b: ByteArray, w: Int, h: Int): Bitmap? {

            try {
                val options = Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeByteArray(b, 0, b.size, options)
                val height = options.outHeight
                val width = options.outWidth
                var inSampleSize = 1

                if (height > h || width > w) {
                    val heightRatio = Math.round(height.toFloat() / h.toFloat())
                    val widthRatio = Math.round(width.toFloat() / w.toFloat())

                    inSampleSize = if (heightRatio < widthRatio)
                        heightRatio
                    else
                        widthRatio

                }
                options.inSampleSize = inSampleSize
                options.inJustDecodeBounds = false
                return BitmapFactory.decodeByteArray(b, 0, b.size, options)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null

        }

        @JvmStatic
        fun getCompressedBm(file: File, w: Int, h: Int): Bitmap? {

            try {
                val options = Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(file.absolutePath, options)
                val height = options.outHeight
                val width = options.outWidth
                var inSampleSize = 1

                if (height > h || width > w) {
                    val heightRatio = Math.round(height.toFloat() / h.toFloat())
                    val widthRatio = Math.round(width.toFloat() / w.toFloat())

                    inSampleSize = if (heightRatio < widthRatio)
                        heightRatio
                    else
                        widthRatio

                }
                options.inSampleSize = inSampleSize
                options.inJustDecodeBounds = false
                return BitmapFactory.decodeFile(file.absolutePath, options)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        @JvmStatic
        fun getCompressedBm(file: File): Bitmap? {

            try {
                val options = Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(file.absolutePath, options)
                val height = options.outHeight
                val width = options.outWidth
                var inSampleSize = 1

                /*if (height > h || width > w)
			{
				final int heightRatio = Math.round((float) height / (float) h);
		        final int widthRatio = Math.round((float) width / (float) w);

		        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

			}*/
                val maxMB = (1024 * 1024 * 2).toLong()

                if (file.length() > maxMB) {
                    val heightRatio = Math.round(file.length().toFloat() / maxMB)

                    inSampleSize = heightRatio

                }
                Log.d("Sample Size======", inSampleSize.toString() + "")
                options.inSampleSize = inSampleSize
                options.inJustDecodeBounds = false
                return BitmapFactory.decodeFile(file.absolutePath, options)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        @JvmStatic
        fun getOrientationFixedImage(f: File, w: Int, h: Int): Bitmap? {

            try {
                val exif = ExifInterface(f.path)
                val orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL)

                var angle = 0

                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    angle = 90
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    angle = 180
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    angle = 270
                }

                val mat = Matrix()
                mat.postRotate(angle.toFloat())

                val bmp = getCompressedBm(f, w, h)
                return Bitmap.createBitmap(bmp!!, 0, 0, bmp.width,
                        bmp.height, mat, true)
            } catch (e: IOException) {
                Log.w("TAG", "-- Error in setting image")
            } catch (oom: OutOfMemoryError) {
                Log.w("TAG", "-- OOM Error in setting image")
            }

            return getCompressedBm(f, w, h)
        }


        @JvmStatic
        fun downLoadImage(urlStr: String): Bitmap? {

            if (!URLUtil.isValidUrl(urlStr))
                return null
            var `is`: InputStream? = null
            var bmp: Bitmap? = null
            try {
                val url = URL(urlStr)
                val conn = url.openConnection()
                conn.connect()
                `is` = conn.getInputStream()

                val opt = Options()
                opt.inScaled = true
                opt.inDither = true

                opt.inTempStorage = ByteArray(16000)

                bmp = BitmapFactory.decodeStream(`is`, null, opt)

                `is`?.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return bmp
        }

        /*
	 * public static String getStringFromPref(Context context, String key){
	 * return context.getSharedPreferences(Constant.PREF_FILE,
	 * Context.MODE_PRIVATE).getString(key, ""); }
	 *
	 * public static boolean getBooleanFromPref(Context context, String key,
	 * boolean defaultVal){ return
	 * context.getSharedPreferences(Constant.PREF_FILE,
	 * Context.MODE_PRIVATE).getBoolean(key, defaultVal); }
	 */

        /** Create a file Uri for saving an image or video  */
        val outputMediaFileUri: Uri
            get() = Uri.fromFile(outputMediaFile)

        /** Create a File for saving an image or video  */
        private// To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.
        // Create the storage directory if it does not exist
        // Create a media file name
        val outputMediaFile: File?
            get() {

                val mediaStorageDir = File(
                        Environment
                                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        "Jaunty Twig")
                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.d("Jaunty Twig", "failed to create directory")
                        return null
                    }
                }
                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss")
                        .format(Date())

                return File(mediaStorageDir.path + File.separator
                        + "JapIMG_" + timeStamp + ".jpg")
            }

        @JvmStatic
        fun CopyStream(`is`: InputStream, os: OutputStream) {
            val buffer_size = 1024
            try {
                val bytes = ByteArray(buffer_size)
                while (true) {
                    val count = `is`.read(bytes, 0, buffer_size)
                    if (count == -1)
                        break
                    os.write(bytes, 0, count)
                }
            } catch (ex: Exception) {
            }

        }

        @Throws(IOException::class)
        @JvmStatic
        fun resolveBitmapOrientation(bitmapFile: File): Int {
            var exif: ExifInterface? = null
            exif = ExifInterface(bitmapFile.absolutePath)

            return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)
        }

        @JvmStatic
        fun applyOrientation(bitmap: Bitmap, orientation: Int): Bitmap {
            var rotate = 0
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 0
                else -> return bitmap
            }

            val w = bitmap.width
            val h = bitmap.height
            val mtx = Matrix()
            mtx.postRotate(rotate.toFloat())
            return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true)
        }

        @JvmStatic
        fun writeBitmapToFile(tempphoto: String, bmp: Bitmap) {
            val fOut: FileOutputStream
            try {
                fOut = FileOutputStream(tempphoto)
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.flush()
                fOut.close()

            } catch (e1: FileNotFoundException) {
                // TODO Auto-generated catch block
                e1.printStackTrace()
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        }

        @JvmStatic
        fun convertStringToFormat(inputDate: String): String {
            val date: Date
            var format = SimpleDateFormat("MMM dd yyyy hh:mm aa")
            try {
                date = format.parse(inputDate)
                format = SimpleDateFormat("dd MMMMM yyyy")
                return format.format(date)
            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
                return ""
            }

        }


        @JvmStatic
        fun validateEditText(et: EditText, requestCode: Int,
                             msg: String): Boolean {
            if (et.text.toString().trim { it <= ' ' }.equals( "")) {
                et.error = "Required Field"
                return false
            } else {

                if (requestCode == 103) {
                    if (!isValidEmail(et.text.toString().trim { it <= ' ' })) {
                        et.error = msg
                        return false
                    } else
                        return true
                } else if (requestCode == 101) {
                    if (et.text.toString().trim { it <= ' ' } == "") {
                        et.error = msg
                        return false
                    } else

                        return true
                } else if (requestCode == 102) {
                    if (!isNumeric(et.text.toString().trim { it <= ' ' })) {
                        et.error = msg
                        return false
                    } else
                        return true
                }
            }
            return true
        }

        @JvmStatic
        fun isEditTextEmpty(et: EditText): Boolean {
            return et.text.toString().trim { it <= ' ' } == ""
        }

        @JvmStatic
        fun validatePassword(password: EditText,
                             confirmPassword: EditText): Boolean {
            var flag = true
            if (password.text.toString().trim { it <= ' ' } == "") {
                password.error = "Required Field"
                flag = false
            }
            if (confirmPassword.text.toString().trim { it <= ' ' } == "") {
                confirmPassword.error = "Required Field"
                flag = false
            }
            if (password.text.toString() != confirmPassword.text.toString()) {
                confirmPassword.error = "Password not match"
                flag = false
            }
            return flag
        }

        @JvmStatic
        fun isPasswordMatch(password: EditText,
                            confirmPassword: EditText): Boolean {
            return password.text.toString() == confirmPassword.text.toString()
        }

        @JvmStatic
        fun isEmailMatch(email: EditText,
                         confirmEmail: EditText): Boolean {
            return email.text.toString() == confirmEmail.text.toString()
        }


        /**
         * This method converts dp unit to equivalent pixels, depending on device
         * density.
         *
         * @param dp
         * A value in dp (density independent pixels) unit. Which we need
         * to convert into pixels
         * @param context
         * Context to get resources and device specific display metrics
         * @return A float value to represent px equivalent to dp depending on
         * device density
         */
        @JvmStatic
        fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return dp * (metrics.densityDpi / 160f)
        }

        /**
         * This method converts device specific pixels to density independent
         * pixels.
         *
         * @param px
         * A value in px (pixels) unit. Which we need to convert into db
         * @param context
         * Context to get resources and device specific display metrics
         * @return A float value to represent dp equivalent to px value
         */
        @JvmStatic
        fun convertPixelsToDp(px: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return px / (metrics.densityDpi / 160f)
        }

        val currentDateTime: String
            get() {
                val dateFormat = SimpleDateFormat("dd-MM-yyyy h:mm:ss a")
                val date = Date()
                return dateFormat.format(date)
            }


        @JvmStatic
        fun formatToYesterdayOrToday(datestr: String): String {

            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

            val dateTime = formatter.parse(datestr);
            val calendar = Calendar.getInstance();
            calendar.setTime(dateTime);
            val today = Calendar.getInstance();
            val yesterday = Calendar.getInstance();

            yesterday.add(Calendar.DATE, -1);

            val timeFormatter = SimpleDateFormat("hh:mm aa");
            val date = SimpleDateFormat("MMM dd hh:mm aa").format(dateTime)

            if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                return "Today " + timeFormatter.format(dateTime);
            } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
                return "Yesterday " + timeFormatter.format(dateTime);
            } else {
                return date;
            }
        }


        fun getDateFromString(strDate: String): Calendar {
            val dateFormat = SimpleDateFormat("dd-MM-yyyy h:mm:ss a")
            val c = Calendar.getInstance()
            try {
                val d = dateFormat.parse(strDate)
                c.time = d
            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
                return c
            }

            return c
        }

        @JvmStatic
        fun getTime(hr: Int, min: Int): String {
            val tme = Time(hr, min, 0)// seconds by default set to zero
            val formatter: Format
            formatter = SimpleDateFormat("h:mm:ss a")
            return formatter.format(tme)
        }

        @JvmStatic
        fun checkDigit(number: Int): String {
            return if (number <= 9) "0$number" else number.toString()
        }

        @JvmStatic
        fun vibrateMe(context: Context) {
            val v = context
                    .getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            // Vibrate for 500 milliseconds
            v.vibrate(100)
        }

        @JvmStatic
        fun roundTo2Decimal(number: Double): Double {
            return Math.round(number * 100.0) / 100.0
        }


        /*public static String getIMEI(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE)
				!= PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(context,new String[Integer.parseInt(Manifest.permission.READ_PHONE_STATE)]);
			return "";
		}
		String device_id = tm.getDeviceId();
		return device_id;
	}*/

        @JvmStatic
        fun getDPValues(context: Context, `val`: Int): Int {
            val r = context.resources
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    `val`.toFloat(), r.displayMetrics).toInt()
        }

        @JvmStatic
        fun openWebUrl(context: Context, url: String) {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            context.startActivity(i)
        }

        @JvmStatic
        fun convertDateFormate(datestr: String): String? {

            val inputPattern = "yyyy-MM-dd HH:mm:ss"
            val outputPattern = "MMM dd-yyyy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)

            var date: Date? = null
            var str: String? = null

            try {
                date = inputFormat.parse(datestr)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return str

        }

        @JvmStatic
        fun convertDateToGMT(datestr: String): String {
            try {
                val fromFormat = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
                val d = fromFormat.parse(datestr)

                val gmtTime = TimeZone.getTimeZone("GMT")
                fromFormat.timeZone = gmtTime
                println("DateTime in GMT : " + fromFormat.format(d))
                return fromFormat.format(d)
                // return d.getTime();
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }

        @JvmStatic
        fun convertGMTToDate(date: String): String {
            try {
                val fromFormat = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
                val formatter = SimpleDateFormat(
                        "dd/MM/yyyy hh:mm:ss")
                formatter.timeZone = TimeZone.getTimeZone("GMT")
                val d = formatter.parse(date)

                return fromFormat.format(d)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }

        @JvmStatic
        fun LogMessage(tag: String, message: String) {
            Log.d(tag, message)
        }

        @JvmStatic
        fun checkPasswordStrength(password: String): Int {
            var strengthPercentage = 0
            val partialRegexChecks = arrayOf(".*[a-z]+.*", // lower
                    ".*[A-Z]+.*", // upper
                    ".*[\\d]+.*", // digits
                    ".*[@#$%]+.*" // symbols
            )


            if (password.matches(partialRegexChecks[0].toRegex())) {
                strengthPercentage += 25
            }
            if (password.matches(partialRegexChecks[1].toRegex())) {
                strengthPercentage += 25
            }
            if (password.matches(partialRegexChecks[2].toRegex())) {
                strengthPercentage += 25
            }
            if (password.matches(partialRegexChecks[3].toRegex())) {
                strengthPercentage += 25
            }
            LogMessage("strengthPercentage", strengthPercentage.toString() + "")

            return strengthPercentage


        }


        @JvmStatic
        fun checkBirthDateValidation(day: Int, month: Int, year: Int): Boolean {
            val cal = Calendar.getInstance()

            cal.set(year, month, day)

            val bthDate = cal.time

            LogMessage("bthDate", bthDate.toString() + "")
            LogMessage("Currewnt", Date().toString() + "")

            val diff1 = Date().compareTo(bthDate)

            LogMessage("diff1", diff1.toString() + "")

            return diff1 > 0
        }

        @JvmStatic
        fun changeDescBack(v: View, color: Int) {
            val sd = v.background.mutate() as GradientDrawable
            sd.setDither(true)
            sd.gradientType = GradientDrawable.LINEAR_GRADIENT

            sd.setColor(color)

            sd.invalidateSelf()
        }


        @JvmStatic
        fun JSONTokener(`in`: String?): String? {
            var `in` = `in`
            // consume an optional byte order mark (BOM) if it exists
            if (`in` != null && `in`.startsWith("\ufeff")) {
                `in` = `in`.substring(1)
            }
            return `in`
        }


        @JvmStatic
        fun changeToString(value: String?): String {
            return value ?: ""
        }

        @JvmStatic
        fun getRoundedCornerBitmap(bitmap: Bitmap): Bitmap {
            val w = bitmap.width
            val h = bitmap.height

            val radius = Math.min(h / 2, w / 2)
            val output = Bitmap.createBitmap(w + 8, h + 8, Config.ARGB_8888)

            val p = Paint()
            p.isAntiAlias = true

            val c = Canvas(output)
            c.drawARGB(0, 0, 0, 0)
            p.style = Style.FILL

            c.drawCircle((w / 2 + 4).toFloat(), (h / 2 + 4).toFloat(), radius.toFloat(), p)

            p.xfermode = PorterDuffXfermode(Mode.SRC_IN)

            c.drawBitmap(bitmap, 4f, 4f, p)
            p.xfermode = null
            p.style = Style.STROKE
            p.color = Color.WHITE
            p.strokeWidth = 3f
            c.drawCircle((w / 2 + 4).toFloat(), (h / 2 + 4).toFloat(), radius.toFloat(), p)

            return output
        }

        @JvmStatic
        fun isGPSEnabled(activity: Activity): Boolean {
            val manager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        }


        @JvmStatic
        fun isGPSEnabled(activity: Context): Boolean {
            val manager = activity
                    .getSystemService(Context.LOCATION_SERVICE) as LocationManager

            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)

        }


        @JvmStatic
        fun appendLog(text: String) {
            val logFile = File("sdcard/bzzzlog.txt")
            if (!logFile.exists()) {
                try {
                    logFile.createNewFile()
                } catch (e: IOException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

            }
            try {
                // BufferedWriter for performance, true to set append to file flag
                val buf = BufferedWriter(FileWriter(logFile,
                        true))
                buf.append(text)
                buf.newLine()
                buf.close()
            } catch (e: IOException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        }

        @JvmStatic
        fun deleteLogs() {
            val logFile = File("sdcard/bzzzlog.txt")
            if (logFile.exists()) {
                logFile.delete()
            }

        }

        @JvmStatic
        fun getIP(context: Context): String {
            try {
                val en = NetworkInterface.getNetworkInterfaces()
                while (en.hasMoreElements()) {
                    val intf = en.nextElement()
                    val enumIpAddr = intf.inetAddresses
                    while (enumIpAddr.hasMoreElements()) {
                        val inetAddress = enumIpAddr.nextElement()
                        if (!inetAddress.isLoopbackAddress) {
                            val ip = Formatter.formatIpAddress(inetAddress.hashCode())
                            Log.i("IP", "***** IP=$ip")
                            return ip
                        }
                    }
                }
            } catch (ex: SocketException) {
                Log.e("IP", ex.toString())
            }

            return ""
        }


        @JvmStatic
        fun getDateFromTimeStamp(timestamp: String): String {

            val calendar = Calendar
                    .getInstance(TimeZone.getTimeZone("GMT"))


            var sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa")
            sdf.timeZone = calendar.timeZone
            try {
                calendar.time = sdf.parse(timestamp)


            } catch (e: ParseException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa")
            calendar.timeZone = TimeZone.getDefault()
            sdf.timeZone = calendar.timeZone

            return sdf.format(calendar.time)

        }

        @JvmStatic
        fun remainingTimeAfterCompareDates(getDateFromServer: String): String {
            val c = Calendar.getInstance()

            val df = SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa")

            var d1: Date? = null
            var d2: Date? = null

            var result = ""

            try {

                val currentDate = df.format(c.time)


                val newDate = getDateFromTimeStamp(getDateFromServer)

                d1 = df.parse(currentDate)
                d2 = df.parse(newDate)

                val diff = d1!!.time - d2!!.time

                val diffSeconds = diff / 1000 % 60
                val diffMinutes = diff / (60 * 1000) % 60
                val diffHours = diff / (60 * 60 * 1000) % 24
                val diffDays = diff / (24 * 60 * 60 * 1000)
                val diffMonths = diffDays / 30
                val diffWeeks = diffDays / 7
                val diffYears = diffDays / 365

                //		 Utils.LogMessage("===============diffDays" , " ============"+diffDays);
                //		 Utils.LogMessage("===============diffHours" , "============"+diffHours);
                //		 Utils.LogMessage("===============diffMinutes" ,"============"+diffMinutes);
                //		 Utils.LogMessage("===============diffSeconds" ," ============"+diffSeconds);


                if (diffYears > 0) {
                    if (diffYears <= 1)
                        result = diffYears.toString() + "yr ago"
                    else
                        result = diffYears.toString() + "yr ago"
                } else if (diffMonths > 0) {
                    if (diffMonths <= 1)
                        result = diffMonths.toString() + "mth ago"
                    else
                        result = diffMonths.toString() + "mth ago"
                } else if (diffWeeks > 0) {
                    if (diffWeeks <= 1)
                        result = diffWeeks.toString() + "w ago"
                    else
                        result = diffWeeks.toString() + "w ago"
                } else if (diffDays > 0) {
                    if (diffDays <= 1)
                        result = diffDays.toString() + "d ago"
                    else
                        result = diffDays.toString() + "d ago"
                } else if (diffHours > 0) {
                    if (diffHours <= 1)
                        result = diffHours.toString() + "hr ago"
                    else
                        result = diffHours.toString() + "hr ago"
                } else if (diffMinutes > 0) {
                    if (diffMinutes <= 1)
                        result = diffMinutes.toString() + "m ago"
                    else
                        result = diffMinutes.toString() + "m ago"
                } else if (diffSeconds > 0) {
                    if (diffSeconds <= 1)
                        result = diffSeconds.toString() + "s ago"
                    else
                        result = diffSeconds.toString() + "s ago"
                }


                //	Utils.LogMessage("==========end=======================", "========end=====");

            } catch (e: Exception) {
                e.printStackTrace()
            }


            return result
        }


        @JvmStatic
        fun makeUserImage(mImageView: ImageView, loadedImage: Bitmap, ctx: Context) {
            val mask = BitmapFactory.decodeResource(ctx.resources, R.mipmap.ic_launcher)
            val result = Bitmap.createBitmap(mask.width, mask.height, Config.ARGB_8888)
            val mCanvas = Canvas(result)
            val paint = Paint(Paint.ANTI_ALIAS_FLAG)
            paint.xfermode = PorterDuffXfermode(Mode.DST_IN)
            mCanvas.drawBitmap(Bitmap.createScaledBitmap(loadedImage, mask.width, mask.height, true), 0f, 0f, null)
            mCanvas.drawBitmap(mask, 0f, 0f, paint)
            paint.xfermode = null
            mImageView.setImageBitmap(result)
            //mImageView.setBackgroundResource(R.drawable.company_image_avtar);
        }


        /**
         * Get a file path from a Uri. This will get the the path for Storage Access
         * Framework Documents, as well as the _data field for the MediaStore and
         * other file-based ContentProviders.
         *
         * @param context The context.
         * @param uri The Uri to query.
         * @author paulburke
         */
        @JvmStatic
        fun getPath(context: Context, uri: Uri): String? {

            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }

                    // TODO handle non-primary volumes
                } else if (isDownloadsDocument(uri)) {

                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))

                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val type = split[0]

                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }

                    val selection = "_id=?"
                    val selectionArgs = arrayOf(split[1])

                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }// MediaProvider
                // DownloadsProvider
            } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {
                return getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
                return uri.path
            }// File
            // MediaStore (and general)

            return null
        }


        /**
         * Get the value of the data column for this Uri. This is useful for
         * MediaStore Uris, and other file-based ContentProviders.
         *
         * @param context The context.
         * @param uri The Uri to query.
         * @param selection (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        @JvmStatic
        fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                          selectionArgs: Array<String>?): String? {

            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(column)

            try {
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }


        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        @JvmStatic
        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        @JvmStatic
        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        @JvmStatic
        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        @JvmStatic
        fun setDynamicHeight(mListView: ListView) {
            val mListAdapter = mListView.adapter
                    ?: // when adapter is null
                    return
            var height = 0
            val desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.width, View.MeasureSpec.UNSPECIFIED)
            for (i in 0 until mListAdapter.count) {
                val listItem = mListAdapter.getView(i, null, mListView)
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                height += listItem.measuredHeight
            }
            val params = mListView.layoutParams
            params.height = height + mListView.dividerHeight * (mListAdapter.count - 1)
            mListView.layoutParams = params
            mListView.requestLayout()
        }

        @JvmStatic
        fun getDigit(digit: String): String {
            return if (getIntFromString(digit) < 10) "0$digit" else digit
        }

        @JvmStatic
        fun getDeviceIpAddress(context: Context): String {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager;
            val activeNetwork = cm.getActiveNetworkInfo();
            val wm = context.getSystemService(Context.WIFI_SERVICE) as WifiManager;

            val connectionInfo = wm.getConnectionInfo();
            val ipAddress = connectionInfo.getIpAddress();
            val ipString = Formatter.formatIpAddress(ipAddress);
            return ipString;
        }

    }

}
