package com.breeders.adaptors

import android.content.Context
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.breeders.R

class AidBidderAdaptor internal constructor(var context: Context) : BaseAdapter() {

    override fun getCount(): Int {
        return 5
    }

    override fun getItem(i: Int): Any? {
        return null
    }

    override fun getItemId(i: Int): Long {
        return 0
    }

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        return  LayoutInflater.from(context).inflate(R.layout.item_bidder,null)
    }
}
