package com.breeders.fragments


import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v4.content.PermissionChecker.checkSelfPermission
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.BuildConfig
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.models.Models
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_add_bid.*
import kotlinx.android.synthetic.main.fragment_add_bid.view.*
import kotlinx.android.synthetic.main.fragment_sub_category.view.*
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AddBidFragment : Fragment(), ServiceEvents {

    lateinit var imagePath: File
    var videoPath: File? = null
    lateinit var fragView: View
    var ads: Models.Ads? = null
    var position = 0

    private var catList = ArrayList<String>()
    private var subCatList = ArrayList<String>()
    private var catListModel = ArrayList<Models.Category>()
    private var subCatListModel = ArrayList<Models.Category>()
    private lateinit var subAdapter: ArrayAdapter<String>
    private lateinit var catAdapter: ArrayAdapter<String>

    private var images = ArrayList<File>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_add_bid, container, false)

        Constant.setFlag(context!!)

        fragView.tv_abbr.text = UserPrefs.getSharedPreferences(context, "countryCode")

        val literals = JSONObject(UserPrefs.getSharedPreferences(context, "literals"))

        fragView.et_phone.hint = literals.optString("Phone Number")
        fragView.et_des.hint = literals.optString("Description")

//        fragView.et_des.imeOptions = EditorInfo.IME_ACTION_DONE;
//        fragView.et_des.setRawInputType(InputType.TYPE_CLASS_TEXT);

        catAdapter = ArrayAdapter(
            context!!,
            R.layout.spinner_item_cat,
            catList
        )


        // Specify the layout to use when the list of choices appears
        catAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        fragView.sp_category.adapter = catAdapter

        fragView.sp_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                getSubCat(catListModel[i].id!!)
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        subAdapter = ArrayAdapter(
            context!!,
            R.layout.spinner_item_cat,
            subCatList
        )

        // Specify the layout to use when the list of choices appears
        subAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        fragView.sp_sub_category.adapter = subAdapter


        fragView.iv_one.setOnClickListener {
            position = 0
            checkCameraPermission(0)
        }
        fragView.iv_two.setOnClickListener {
            position = 1
            checkCameraPermission(0)
        }
        fragView.iv_three.setOnClickListener {
            position = 2
            checkCameraPermission(0)
        }
        fragView.iv_four.setOnClickListener {
            position = 3
            checkCameraPermission(0)
        }
        fragView.iv_video.setOnClickListener {
            position = 4
            checkCameraPermission(1)
        }
        fragView.btn_add_ads.setOnClickListener {
            if (!isEmpty())
                uploadAds()
            //              Utils.showDialog(context!!,"","All Ok")
        }
        getCat()

        val argument = arguments

        if (argument != null) {
            ads = argument.getSerializable("ads") as Models.Ads
            setUI(ads!!)
        }
        return fragView;
    }

    private fun setUI(ads: Models.Ads) {
        fragView.et_phone.setText(ads.phone)
        fragView.et_des.setText(ads.description)
        fragView.btn_add_ads.text = getString(R.string.update_ads)
    }

    private fun isEmpty(): Boolean {
        var blackImage = true

        for (image in images) {
            if (image != null)
                blackImage = false
        }

        return when {
            fragView.et_phone.text.isEmpty() -> {
                fragView.et_phone.error = getString(R.string.please_enter_phone_number)
                true
            }
            fragView.et_des.text.isEmpty() -> {
                fragView.et_des.error = getString(R.string.description)
                true
            }
//            blackImage -> {
//                Utils.showDialog(context!!, "", getString(R.string.all_images))
//                true
//            }

            !fragView.cb_terms.isChecked -> {
                Utils.showDialog(context!!, "", getString(R.string.accept_terms_amp_condition))
                true
            }
            else -> false
        }
    }

    private fun selectImage() {
        val items = arrayOf<CharSequence>(
            getString(R.string.camera),
            getString(R.string.gallery), getString(R.string.cancel)
        )
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(getString(R.string.select_image))
        builder.setItems(items) { dialog, item ->
            when {
                items[item] == getString(R.string.camera) -> openCameraNow()
                items[item] == getString(R.string.gallery) -> {
                    val pickPhoto =
                        Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 103);//one can be replaced with any action code
                }
                items[item] == getString(R.string.cancel) -> dialog.dismiss()
            }
        }
        builder.show()
    }

    private fun selectVideo() {
        val intent = Intent();
        intent.type = "video/*";
        intent.action = Intent.ACTION_GET_CONTENT;
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_video)), 201);
    }

    private fun checkCameraPermission(type: Int) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (checkSelfPermission(context!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                    context!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                    context!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    101
                )
            } else {
                if (type == 0)
                    selectImage()
                else selectVideo()
            }
        else {
            if (type == 0)
                selectImage()
            else selectVideo()
        }
    }

    private fun openCameraNow() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity!!.packageManager) != null) {
            // Create the File where the photo should go
            val photoFile = getImageFileUri()
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFile)
                startActivityForResult(takePictureIntent, 102)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage()
            } else {
                Toast.makeText(context, "camera permission denied", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun getImageFileUri(): Uri {

        imagePath =
                File(Environment.getExternalStorageDirectory(), "" + Calendar.getInstance().time.time + " photo.jpg")

        if (!imagePath.exists()) {
            try {
                imagePath.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        Log.e("AddAds", "Image Path => $imagePath")

        // Create an File Uri
        return FileProvider.getUriForFile(context!!, BuildConfig.APPLICATION_ID + ".provider", imagePath)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 102) {

                images.add(imagePath)

                val myBitmap = BitmapFactory.decodeFile(imagePath.getAbsolutePath())

                when (position) {
                    0 -> fragView.iv_one.setImageBitmap(myBitmap)
                    1 -> fragView.iv_two.setImageBitmap(myBitmap)
                    2 -> fragView.iv_three.setImageBitmap(myBitmap)
                    3 -> fragView.iv_four.setImageBitmap(myBitmap)
                }
            } else if (requestCode == 103) {
                val selectedImageUri = data!!.data;
                val selectedFilePath = Utils.getPath(activity!!.applicationContext, selectedImageUri);
                val profileFile = File(selectedFilePath);
                if (selectedFilePath != null)
                    images.add(profileFile)

                Log.e("AddAds", "Image Path => " + profileFile.absolutePath)
                // MEDIA GALLERY

                when (position) {
                    0 -> fragView.iv_one.setImageURI(selectedImageUri);
                    1 -> fragView.iv_two.setImageURI(selectedImageUri);
                    2 -> fragView.iv_three.setImageURI(selectedImageUri);
                    3 -> fragView.iv_four.setImageURI(selectedImageUri);
                }
            } else if (requestCode == 201) {

                val selectedImageUri = data!!.data;
                val selectedFilePath = Utils.getPath(activity!!.applicationContext, selectedImageUri);
                val profileFile = File(selectedFilePath);

                val thumb = ThumbnailUtils.createVideoThumbnail(
                    selectedFilePath,
                    MediaStore.Images.Thumbnails.MINI_KIND
                );

                Log.e("AddAds", "Video Path => $profileFile")

                if (selectedFilePath != null) {
                    videoPath = profileFile
                    fragView.iv_video.setImageBitmap(thumb);
                }
            }
        }
    }

    private fun getLangParam(): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        return param
    }

    private fun getCat() {
        if (Utils.isOnline(context!!)) {

            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.category, getLangParam(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }
    //  (abbr=en,ar,ru,phone,description,shop_categorie,subcat_id,mobile_id,video,image,key)

    private fun getAdsParam(): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("abbr", UserPrefs.getSharedPreferences(context, "countryCode"))
        param.set("phone", fragView.et_phone.text.toString())
        param.set("description", fragView.et_des.text.toString())
        param.set("shop_categorie", catListModel[fragView.sp_category.selectedItemPosition].id!!)
        param.set("subcat_id", subCatListModel[fragView.sp_sub_category.selectedItemPosition].id!!)
        param.set("mobile_id", UserPrefs.getSharedPreferences(context, "mobile_id"))
        param.set("country", UserPrefs.getSharedPreferences(context, "countryCode"))
        return param
    }

    private fun uploadAds() {

        if (Utils.isOnline(context!!)) {

            val url = if (ads != null) Constant.updateAds else Constant.addAds


            val services = GetServices(this, context)
            try {
                if (images.size > 0)
                    services.multiPartRestCallAsync(url, getAdsParam(), images, videoPath)
                else
                    services.RestCallAsync(url, getAdsParam(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getParam(id: String?): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        param.set("shop_categorie", id!!)
        return param
    }

    private fun getSubCat(id: String) {
        val bzzzLoginServices = GetServices(this, context)
        try {
            bzzzLoginServices.RestCallAsync(Constant.categorySubList, getParam(id), false)
        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
    }


    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {

        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {
            when {
                methodName.equals(Constant.category, false) -> {

                    catList.clear()
                    catListModel.clear()

                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        catListModel.add(
                            Gson().fromJson(
                                dataArray.optJSONObject(i).toString(),
                                Models.Category::class.java
                            )
                        )
                        catList.add(dataArray.optJSONObject(i).optString("title"))
                    }

                    catAdapter.notifyDataSetChanged()
                }
                methodName.equals(Constant.categorySubList, false) -> {

                    subCatList.clear()
                    subCatListModel.clear()

                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        val subCat = Gson().fromJson(
                            dataArray.optJSONObject(i).toString(),
                            Models.Category::class.java
                        )
                        subCatListModel.add(subCat)
                        subCatList.add(subCat.title!!)
                    }

                    subAdapter.notifyDataSetChanged()
                }
                methodName.equals(Constant.addAds) ||  methodName.equals(Constant.updateAds)-> {
                    Utils.showDialog(
                        context!!,
                        "",
                        getString(R.string.pls_wait_for_ads),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            fragmentManager!!.beginTransaction().addToBackStack(null)
                                .replace(R.id.frag_container, AddBidFragment()).commit()
                        })
                }
            }
        } else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }


}
