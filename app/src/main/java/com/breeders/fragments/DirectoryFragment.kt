package com.breeders.fragments


import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.adandroid.mobileinfo.utils.Utils
import com.adandroid.volleyrest.GetServices
import com.adandroid.volleyrest.ServiceEvents
import com.breeders.R
import com.breeders.Utils.Constant
import com.breeders.Utils.UserPrefs
import com.breeders.activities.WebViewActivity
import com.breeders.adaptors.CountryAdaptor
import com.breeders.adaptors.DirectoryAdaptor
import com.breeders.models.Country
import com.breeders.models.Models
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_directory.view.*
import kotlinx.android.synthetic.main.item_country.view.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DirectoryFragment : Fragment(), ServiceEvents {


    private var directoryList = ArrayList<Models.Directory>()
    private var mainDirectoryList = ArrayList<Models.Directory>()
    lateinit var directoryAdaptor: DirectoryAdaptor
    lateinit var fragView: View
    private var countries = java.util.ArrayList<Country>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_directory, container, false)

        Constant.setFlag(context!!)

        directoryAdaptor = DirectoryAdaptor(context!!, mainDirectoryList)

        fragView.lv_directory_list.adapter = directoryAdaptor

        Picasso.get().load("https://4breeders.com/banner4.png").into(fragView.iv_banner)

        fragView.iv_banner.setOnClickListener {
            val url = "https://4breeders.com/banner4.php"
//            val i = Intent(Intent.ACTION_VIEW)
//            i.data = Uri.parse(url)
//            startActivity(i)

            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra("url", url)
            startActivity(intent)
        }



        fragView.lv_directory_list.setOnItemClickListener { adapterView, view, i, l ->
            val directoryDetail = DirectoryDetailsFragment()
            val argument = Bundle()
            argument.putString("id", directoryList[i].id)
            directoryDetail.arguments = argument
            fragmentManager!!.beginTransaction().addToBackStack(null)
                .replace(R.id.frag_container, directoryDetail).commit()
        }

        fragView.tl_country_tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{

            override fun onTabReselected(p0: TabLayout.Tab?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    0 -> {
                        getDirectory()
                    }
                    else -> {
                        mainDirectoryList = directoryList.filter { filter(it, countries[tab.position - 1].name!!) } as ArrayList<Models.Directory>
                        Log.d("Directory", " Directory = $mainDirectoryList")

                        directoryAdaptor = DirectoryAdaptor(context!!, mainDirectoryList)
                        fragView.lv_directory_list.adapter = directoryAdaptor
                    }
                }
            }
        })

        getCountry()
        return fragView;
    }

    private fun getCountry() {

        if (Utils.isOnline(context!!)) {
            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.countryDirectory, null, true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    private fun getParam(): Map<String, String> {
        val param = HashMap<String, String>()
        param.set("lang", UserPrefs.getSharedPreferences(context, "language"))
        return param
    }

    private fun getDirectory() {
        if (Utils.isOnline(context!!)) {

            val bzzzLoginServices = GetServices(this, context)
            try {
                bzzzLoginServices.RestCallAsync(Constant.getdirectory, getParam(), true)
            } catch (e: Exception) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }
        } else
            Utils.showDialog(
                context!!, "",
                getString(R.string.no_network)
            )
    }

    override fun StartedRequest() {

    }

    override fun Finished(methodName: String, response: String) {


        val jsonObject = JSONObject(response)

        if (jsonObject.optString("status") == "1") {


            when {
                methodName.contains(Constant.getdirectory) -> {
                    this.mainDirectoryList.clear()

                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        this.mainDirectoryList.add(
                            Gson().fromJson(
                                dataArray.optJSONObject(i).toString(),
                                Models.Directory::class.java
                            )
                        )
                    }
                    this.directoryList = this.mainDirectoryList
                    this.directoryAdaptor.notifyDataSetChanged()
                }

                methodName.contains(Constant.countryDirectory) -> {

                    countries.clear()
                    //        countriesCode.clear()

                    //       countries.add(getString(R.string.select_country))
                    //       countriesCode.add("+0")

                    val dataArray = jsonObject.optJSONArray("data")

                    for (i in 0 until dataArray.length()) {
                        countries.add(
                            Gson().fromJson(
                                dataArray.optJSONObject(i).toString(),
                                Country::class.java
                            )
                        )

                        //           countriesCode.add(dataArray.optJSONObject(i).optString("abbr"))
                    }

                    countries.sortBy { selector(it) }

                    setTabLayout(countries)

                 //   getDirectory()
                }
            }
        } else {
            Utils.showDialog(
                context!!, "",
                jsonObject.optString("message")
            )
        }

    }

    override fun FinishedWithException(methodName: String?, error: String?) {
        Utils.showDialog(
            context!!, "", error!!
        )
    }

    override fun EndedRequest() {

    }

    fun selector(con: Country): String? = con.name

    fun filter(dir: Models.Directory, country: String): Boolean = dir.country.equals(country,false)

    fun setTabLayout(countries: ArrayList<Country>) {
        var all = Country("101", getString(R.string.all), "http://all", "0")
        fragView.tl_country_tab.addTab(fragView.tl_country_tab.newTab().setCustomView(getTabView(all)))
        for (country in countries.reversed()) {
            fragView.tl_country_tab.addTab(
                fragView.tl_country_tab.newTab().setCustomView(
                    getTabView(
                        country
                    )
                )
            )
        }
    }

    fun getTabView(country: Country): View {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        val view = LayoutInflater.from(context).inflate(R.layout.custom_country_tab, null)
        view.tv_con_name.text = country.name!!.toUpperCase()

        Picasso.get()
            .load(country.flag)
            .into(view.iv_con_image)

        if (country.name.equals("all",true))
            view.iv_con_image.visibility = View.GONE

        return view
    }

}
