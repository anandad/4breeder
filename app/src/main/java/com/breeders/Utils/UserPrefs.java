package com.breeders.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;

public class UserPrefs {
	private static final String KEY_NOTIFICATIONS = "notifications";

	public static SharedPreferences sp;

	private static void init(Context context) {
		sp = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static String getSharedPreferences(Context con, String key) {
		init(con);
		return sp.getString(key, "");


	}

	public static void setSharedPreferences(Context con, String key, String value) {
		init(con);
		Editor edit = sp.edit();
		edit.putString(key, value);
		edit.commit();

	}

	public static int getIntSharedPreferences(Context con, String key) {
		init(con);
		return sp.getInt(key, 0);
	}

	public static void setIntSharedPreferences(Context con, String key, int value) {
		init(con);
		Editor edit = sp.edit();
		edit.putInt(key, value);
		edit.commit();
	}

	public static boolean getBooleanSP(Context con, String key) {
		init(con);
		return sp.getBoolean(key, false);
	}

	public static void setBooleanSP(Context con, String key, boolean value) {
		init(con);
		Editor edit = sp.edit();
		edit.putBoolean(key, value);
		edit.commit();
	}


	public static void resetSharedPreferences(Context con) {
		init(con);
		Editor edit = sp.edit();
		edit.clear().commit();

	}

	public void addNotification(String notification) {

		// get old notifications
		String oldNotifications = getNotifications();

		if (oldNotifications != null) {
			oldNotifications += "|" + notification;
		} else {
			oldNotifications = notification;
		}
		Editor edit = sp.edit();
		edit.putString(KEY_NOTIFICATIONS, oldNotifications);
		edit.commit();
	}

	public String getNotifications() {
		return sp.getString(KEY_NOTIFICATIONS, null);
	}

	public void clear() {
		Editor edit = sp.edit();
		edit.clear();
		edit.commit();
	}

}
