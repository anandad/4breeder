package com.breeders.models

class Country {
    var id: String? = null
    var abbr: String? = null
    var name: String? = null
    var currency: String? = null
    var flag: String? = null

    constructor(id: String,name: String, flag: String, abbr: String){
        this.id = id
        this.name = name;
        this.flag = flag;
        this.abbr = abbr;
    }

    override fun toString(): String {
        return "Country(id=$id, abbr=$abbr, name=$name, currency=$currency, flag=$flag)"
    }


}
